<?php

	namespace Sixnapps\AnalyticBundle\DependencyInjection;

	use Symfony\Component\Config\Definition\Builder\TreeBuilder;
	use Symfony\Component\Config\Definition\ConfigurationInterface;

	class Configuration implements ConfigurationInterface
	{
		/**
		 * @return TreeBuilder
		 */
		public function getConfigTreeBuilder()
		{
			$treeBuilder = new TreeBuilder('sixnapps_analytic' );
			// Keep compatibility with symfony/config < 4.2
			if (!method_exists($treeBuilder, 'getRootNode')) {
				$rootNode    = $treeBuilder->root( 'sixnapps_analytic' );
			} else {
				$rootNode = $treeBuilder->getRootNode();
			}

			$rootNode
				->children()
					->arrayNode('trafic')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard'] )->end()
							->scalarNode( 'titre' )->defaultValue( 'Trafic' )->end()
							->scalarNode( 'icon' )->defaultValue( 'fa fa-bar-chart' )->end()
							->scalarNode( 'disabled' )->defaultValue( false )->end()
						->end()
					->end()
					->arrayNode('contenu')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard_content','sixnapps_analytic_dashboard_page'] )->end()
							->scalarNode( 'titre' )->defaultValue( 'Contenu' )->end()
							->scalarNode( 'icon' )->defaultValue( 'fa fa-file' )->end()
							->scalarNode( 'disabled' )->defaultValue( false )->end()
						->end()
					->end()
					->arrayNode('sources')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard_sources'] )->end()
							->scalarNode( 'titre' )->defaultValue( 'Sources' )->end()
							->scalarNode( 'icon' )->defaultValue( 'fa fa-random' )->end()
							->scalarNode( 'disabled' )->defaultValue( false )->end()
						->end()
					->end()
					->arrayNode('demographique')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard_demographique'] )->end()
							->scalarNode( 'titre' )->defaultValue( 'Démographie' )->end()
							->scalarNode( 'icon' )->defaultValue( 'fa fa-desktop' )->end()
							->scalarNode( 'disabled' )->defaultValue( false )->end()
						->end()
					->end()
					->arrayNode('map')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard_map'] )->end()
							->scalarNode( 'titre' )->defaultValue( 'Géographie' )->end()
							->scalarNode( 'icon' )->defaultValue( 'fa fa-map-marker' )->end()
							->scalarNode( 'disabled' )->defaultValue( false )->end()
						->end()
					->end()
					->arrayNode('graph')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard_graph'] )->end()
							->scalarNode( 'titre' )->defaultValue( 'Synapses' )->end()
							->scalarNode( 'icon' )->defaultValue( 'fa fa-code-fork' )->end()
							->scalarNode( 'disabled' )->defaultValue( false )->end()
						->end()
					->end()
					->arrayNode('parameters')
						->addDefaultsIfNotSet()
							->children()
								->scalarNode( 'routes' )->defaultValue( ['sixnapps_analytic_dashboard_parameters'] )->end()
								->scalarNode( 'titre' )->defaultValue( 'Paramètres' )->end()
								->scalarNode( 'icon' )->defaultValue( 'fa fa-cog' )->end()
								->scalarNode( 'disabled' )->defaultValue( false )->end()
							->end()
						->end()
					->arrayNode('custom')
						->addDefaultsIfNotSet()
						->children()
							->scalarNode( 'routes' )->defaultValue( '' )->end()
							->scalarNode( 'titre' )->defaultValue( '' )->end()
							->scalarNode( 'icon' )->defaultValue( '' )->end()
							->scalarNode( 'disabled' )->defaultValue( true )->end()
						->end()
					->end()
				->end()
			;

			return $treeBuilder;
		}
	}
