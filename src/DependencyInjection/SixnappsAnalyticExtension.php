<?php

	namespace Sixnapps\AnalyticBundle\DependencyInjection;
	
	use Symfony\Component\Config\FileLocator;
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
	use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
	use Symfony\Component\HttpKernel\DependencyInjection\Extension;
	
	/**
	 * Class SixnappsAnalyticExtension
	 *
	 * @package Sixnapps\AnalyticBundle\DependencyInjection
	 */
	class SixnappsAnalyticExtension extends Extension
	{
		/**
		 * @param array            $configs
		 * @param ContainerBuilder $container
		 *
		 * @throws \Exception
		 */
		public function load(array $configs, ContainerBuilder $container)
		{
			$config = $this->processConfiguration(new Configuration(), $configs);
			$loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
			$loader->load('services.yaml');
			$loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
			$loader->load('command.xml');
			$container->setParameter('sixnapps_analytic', $config);
		}
		
		
		/**
		 * @return string
		 */
		public function getAlias()
		{
			return 'sixnapps_analytic';
		}
		
		
		/**
		 * @return string
		 */
		public function getNamespace()
		{
//			return 'http://helios-ag.github.io/schema/dic/fm_summernote';
			return 'sixnapps_analytic';
		}
	}
