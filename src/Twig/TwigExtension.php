<?php

	namespace Sixnapps\AnalyticBundle\Twig;

	use Symfony\Component\DependencyInjection\ContainerInterface;

	/**
	 * Class TwigExtention
	 *
	 * @package Sixnapps\AnalyticBundle\Twig
	 */
	class TwigExtension extends \Twig_Extension
	{
		/**
		 * @var ContainerInterface
		 */
		private $container;


		/**
		 * TwigExtention constructor.
		 *
		 * @param ContainerInterface $container
		 */
		public function __construct(ContainerInterface $container)
		{
			$this->container = $container;
		}
		
		
		/**
		 * @return array|\Twig_Function[]
		 */
		public function getFunctions()
		{
			return [
				new \Twig_SimpleFunction( 'analytic', [ $this, 'analytic' ] ),
				new \Twig_SimpleFunction( 'tracker', [ $this, 'tracker' ], [ 'is_safe' => [ 'html' ] ] ),
			];
		}
		
		
		/**
		 * @return mixed
		 */
		public function analytic( )
		{
			return $this->container->getParameter( 'sixnapps_analytic' );
		}
		
		
		/**
		 * @return string
		 */
		public function tracker(){
			return  "<script>" .
					"let _glance = Routing.generate('sixnapps.analytic.tracker', null, true);" .
					"let script = document.createElement('script');" .
					"script.src = '/bundles/sixnappsanalytic/js/glance.js';" .
					"document.getElementsByTagName('head')[0].appendChild(script);" .
					"</script>";
		}
	}



