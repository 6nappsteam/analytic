<?php
	
	namespace Sixnapps\AnalyticBundle\Controllers;
	
	use App\Entity\AnalyticDomains;
	use Doctrine\DBAL\DBALException;
	use Doctrine\ORM\OptimisticLockException;
	use Doctrine\ORM\ORMException;
	use Sixnapps\AnalyticBundle\Exception\RedirectException;
	use Sixnapps\AnalyticBundle\Form\Type\DomainType;
	use Sixnapps\AnalyticBundle\Services\ContentServices;
	use Sixnapps\AnalyticBundle\Services\DemographicsServices;
	use Sixnapps\AnalyticBundle\Services\GraphServices;
	use Sixnapps\AnalyticBundle\Services\MapServices;
	use Sixnapps\AnalyticBundle\Services\MobileServices;
	use Sixnapps\AnalyticBundle\Services\PageDetailsServices;
	use Sixnapps\AnalyticBundle\Services\SourceDetailsServices;
	use Sixnapps\AnalyticBundle\Services\SourcesServices;
	use Sixnapps\AnalyticBundle\Services\TrafficServices;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use DateTime;
	
	/**
	 * Class DashboardController
	 *
	 * @package Sixnapps\AnalyticBundle\Controllers
	 */
	class DashboardController extends AbstractController
	{
		private $traffic;
		private $content;
		private $sources;
		private $source_details;
		private $demographics;
		private $map;
		private $page_details;
		private $mobile;
		private $graph;
		private $domain_id;
		private $host;
		
		
		/**
		 * DashboardController constructor.
		 *
		 * @param TrafficServices       $traffic
		 * @param ContentServices       $content
		 * @param SourcesServices       $sources
		 * @param SourceDetailsServices $source_details
		 * @param DemographicsServices  $demographics
		 * @param MapServices           $map
		 * @param PageDetailsServices   $page_details
		 * @param MobileServices        $mobile
		 * @param GraphServices         $graph
		 */
		public function __construct(
			TrafficServices $traffic,
			ContentServices $content,
			SourcesServices $sources,
			SourceDetailsServices $source_details,
			DemographicsServices $demographics,
			MapServices $map,
			PageDetailsServices $page_details,
			MobileServices $mobile,
			GraphServices $graph )
		{
			$this->traffic        = $traffic;
			$this->content        = $content;
			$this->sources        = $sources;
			$this->source_details = $source_details;
			$this->demographics   = $demographics;
			$this->map            = $map;
			$this->page_details   = $page_details;
			$this->mobile         = $mobile;
			$this->graph          = $graph;
			$this->domain_id      = NULL;
			$this->host           = NULL;
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function show( Request $request )
		{
			$this->checkDomain( $request );
			return $this->render( '@SixnappsAnalytic/dashboard/tab_1.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
				],
				'tab1' => TRUE,
			] );
		}
		
		
		/**
		 * @param $request
		 *
		 * @throws RedirectException
		 */
		private function checkDomain( $request )
		{
			//Get allowed domains
			$host = parse_url( str_replace( [ 'http://', 'https://', 'www.' ], '',
				$request->server->get( 'SERVER_NAME' ) ) );
			
			$currentHost = $this->getDoctrine()->getRepository( AnalyticDomains::class )->findOneBy( [
				'host' => $host,
			] )
			;
			
			if ( !empty( $currentHost ) ) {
				$this->domain_id = $currentHost->getId();
				$this->host      = $currentHost->getHost();
			} else {
				throw new RedirectException(
					new RedirectResponse(
						$this->generateUrl( 'sixnapps_analytic_dashboard_parameters' )
					)
				);
			}
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function contentShow( Request $request )
		{
			$this->checkDomain( $request );
			
			return $this->render( '@SixnappsAnalytic/dashboard/tab_2_content.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
					'content' => $this->content->getDatas( $this->domain_id, $this->host, 7 ),
				],
			] );
		}
		
		
		/**
		 * @param Request $request
		 * @param         $page
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function pageShow( Request $request, $page )
		
		{
			$this->checkDomain( $request );
			
			return $this->render( '@SixnappsAnalytic/dashboard/tab_2_page.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
					'page'    => $this->page_details->getDatas( $this->domain_id, $page ),
				],
			] );
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function sourcesShow( Request $request )
		{
			$this->checkDomain( $request );
			
			return $this->render( '@SixnappsAnalytic/dashboard/tab_3_sources.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
					'sources' => $this->sources->getDatas( $this->domain_id, $this->host, 7 ),
				],
			] );
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function demographicShow( Request $request )
		{
			$this->checkDomain( $request );
			return $this->render( '@SixnappsAnalytic/dashboard/tab_4_demographique.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
					'geo'     => $this->demographics->getDatas( $this->domain_id, 7 ),
				],
			] );
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function mapShow( Request $request )
		{
			$this->checkDomain( $request );
			return $this->render( '@SixnappsAnalytic/dashboard/tab_5_map.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
					'map'     => $this->map->getDatas( $this->domain_id, 7 ),
					'country' => [
						'alpha2code' => 'alpha2code',
						'name'       => 'name',
						'perc'       => 0.3,
						'count'      => 30,
					],
					'city'    => [
						'alpha2code' => 'alpha2code',
						'name'       => 'name',
						'country'    => 'country',
						'perc'       => 0.3,
						'count'      => 30,
					],
				],
			] );
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function graphShow( Request $request )
		{
			$this->checkDomain( $request );
			
			return $this->render( '@SixnappsAnalytic/dashboard/tab_6_graph.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
				],
			] );
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return Response
		 * @throws DBALException
		 * @throws ORMException
		 * @throws OptimisticLockException
		 */
		public function parameters( Request $request )
		{
			$em     = $this->get( 'doctrine.orm.entity_manager' );
			$domain = new AnalyticDomains();
			$now    = new DateTime();
			$domain
				->setDate( $now )
				->setHost( $request->getHost() )
				->setName( 'created by SF' )
			;
			
			$form = $this->createForm( DomainType::class, $domain );
			$form->handleRequest( $request );
			
			if ( $form->isSubmitted() ) {
				if ( $form->isValid() ) {
					if (class_exists('App\Entity\AnalyticDomains')){
						$existDomains = $em->getRepository( AnalyticDomains::class )->findOneBy( [
							'host' => $domain->getHost(),
						] )
						;
						if ( empty( $existDomains ) ) {
							$em->persist( $domain );
							$em->flush();
							$this->addFlash( 'success', 'Domain enregistré avec succès' );
							$url = $this->generateUrl( 'sixnapps_analytic_dashboard_parameters' );
							return $this->redirect( $url );
						}
					} else {
					
					}
				}
			}
			return $this->render( '@SixnappsAnalytic/dashboard/tab_7_parameters.html.twig', [
				'data' => [
					'traffic' => $this->traffic->getDatas( $this->domain_id ),
				],
				'form' => $form->createView(),
			] );
		}
		
		
		/**
		 * @param Request $request
		 * @param int     $day
		 *
		 * @return JsonResponse
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function graphData( Request $request, $day = 7 )
		{
			$this->checkDomain( $request );
			
			$data = $this->graph->getDatas( $this->domain_id, $day );
			
			return new JsonResponse( $data );
		}
		
		
		/**
		 * @param Request $request
		 *
		 * @return JsonResponse
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function trafficAjax( Request $request )
		{
			$this->checkDomain( $request );
			
			return new JsonResponse( $this->traffic->getDatas( $this->domain_id ) );
		}
		
		
		/**
		 * @param Request $request
		 * @param         $page
		 *
		 * @return JsonResponse
		 * @throws DBALException
		 * @throws RedirectException
		 */
		public function pageTrafficAjax( Request $request, $page )
		{
			$this->checkDomain( $request );
			return new JsonResponse( $this->page_details->getDatas( $this->domain_id, $page ) );
		}
	}
