<?php

	namespace Sixnapps\AnalyticBundle\Controllers;

	use App\Entity\AnalyticDomains;
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;

	/**
	 * Class ScriptController
	 *
	 * @package Sixnapps\AnalyticBundle\Controllers
	 */
	class ScriptController extends AbstractController
	{
		/**
		 * @var int
		 */
		private $domain_id = -1;

		/**
		 * @var string
		 */
		private $host = '';

		/**
		 * @var array
		 */
		private $data = [];


		/**
		 * @param Request $request
		 *
		 * @return Response
		 */
		public function tracker(Request $request)
		{
			$this->checkDomain( $request );
			$ref_host = $request->get( 'rf' ); //parse_url( $request->get( 'rf' ) );

			$content = urldecode( $this->setTrim( $request->get( 'content' ) ) );
			$content = explode( '?', $content );
			$content = $content[ 0 ];

			$this->data = [
				'req'           => $request->server->get( 'REQUEST_URI' ),
				'ip'            => $request->server->get( 'REMOTE_ADDR' ),
				'user_agent'    => $request->server->get( 'HTTP_USER_AGENT' ),

				//				'content'       => $request->server->get( 'SERVER_NAME' ),
				'content'       => $content,
				'content_title' => urldecode( $this->setTrim( $request->get( 'ct' ) ) ),

				'browser'    => $this->setTrim( $request->get( 'b' ) ),
				'os'         => $this->setTrim( $request->get( 'os' ) ),
				'type'       => $this->setTrim( $request->get( 't' ) ),
				'lang'       => $this->setTrim( $request->get( 'l' ) ),
				'resolution' => $this->setTrim( $request->get( 'r' ) ),

				'unique' => $request->get( 'u' ),
				'visit'  => $request->get( 'v' ),

				'referrer'       => urldecode( $this->setTrim( $request->get( 'rf' ) ) ), //direct!
				'referrer_title' => 'x',
				'ref_host'       => $this->host,

				'dom_time'   => (int) $this->setTrim( $request->get( 'dt' ) ),
				'visit_time' => (int) $this->setTrim( $request->get( 'pt' ) ),
			];
			if ( $request->get( 'click', FALSE ) ) {
				$this->click( $request->get( 'ln' ) );
			}
			else {
				$this->pageView();
			}
			return new Response( '<body></body>' );
		}


		/**
		 * @param $request
		 */
		private function checkDomain($request)
		{
			//Get allowed domains
			$host = parse_url( str_replace( [ 'http://', 'https://', 'www.' ], '', $request->server->get( 'SERVER_NAME' ) ) );

			$currentHost = $this->getDoctrine()->getRepository( AnalyticDomains::class )->findOneBy( [
				'host' => $host,
			] )
			;

			if ( !empty( $currentHost ) ) {
				$this->domain_id = $currentHost->getId();
				$this->host      = $currentHost->getHost();
			}
			else {
//				die( 'X-Glance-Message: Error Bad Domain : ' . $request->server->get( 'SERVER_NAME' ) );
//				header( 'X-Glance-Message: Error Bad Domain' );
//				exit();
			}
		}


		/**
		 * @param null $str
		 *
		 * @return string
		 */
		private function setTrim($str = NULL)
		{
			return ( isset( $str ) && !empty( $str ) ) ? trim( $str ) : 'x';
		}


		/**
		 * @param $link
		 */
		private function click($link)
		{
			$c = NULL;

			$is_external = isset( $_GET[ 'ex' ] ) && $_GET[ 'ex' ] == '1';

			$q = "INSERT INTO page_stats (domain_id, date, url_id, visit_time) VALUES ( " .
				 ":domain_id, now(), (SELECT url_id FROM url_lookup WHERE url = :content), :visit_time) " .
				 "ON DUPLICATE KEY UPDATE visit_time = (visit_time + :visit_time)/2;";

			//if external, log click
			if ( $is_external ) {
				$q .= "INSERT IGNORE INTO url_lookup (url, host) VALUES (:link, :link_host);";

				$q .= "INSERT INTO url_map (domain_id, `date`, url_from, url_to) VALUES (" .
					  ":domain_id, " .
					  "now(), " .
					  "( SELECT url_id FROM url_lookup WHERE url = :content ), " .
					  "( SELECT url_id FROM url_lookup WHERE url = :link ) " .
					  ") ON DUPLICATE KEY UPDATE count = count + 1;";
			}

			$c = $this->getDoctrine()->getConnection()->prepare( $q );

			$c->bindValue( 'content', $_SERVER[ 'HTTP_REFERER' ] );
			$c->bindValue( 'domain_id', $this->domain_id );
			$c->bindValue( 'visit_time', $this->data[ 'visit_time' ] );

			if ( $is_external ) {
				$link_host = parse_url( $link );
				$link_host = str_replace( 'www.', '', $link_host[ 'host' ] );
				$c->bindValue( 'link', $link );
				$c->bindValue( 'link_host', $link_host );
			}

			$c->execute();
		}


		/**
		 *
		 */
		private function pageView()
		{
			$this->searchEngineTerms();

			$date     = date( 'Y-m-d' );
			$fulldate = date( 'Y-m-d H:00:00' );

			//if visit
			if ( $this->data[ 'visit' ] == 1 ) {
				//new visit
				$q = '';

				//is unique
				if ( $this->data[ 'unique' ] == '1' ) {
					//unique visitor
					$q .= "INSERT INTO traffic( domain_id, `date`, visits, uniques ) VALUES(:domain_id, '$fulldate', 1, 1 ) " .
						  "ON DUPLICATE KEY UPDATE views = views + 1, visits = visits + 1, uniques = uniques + 1;";
				}
				else {
					//returning visitor
					$q .= "INSERT INTO traffic( domain_id, `date`, visits ) VALUES(:domain_id, '$fulldate', 1) " .
						  "ON DUPLICATE KEY UPDATE views = views + 1, visits = visits + 1;";
				}

				//add to referrer table
				$q .= "INSERT IGNORE  INTO url_lookup( url, host, title ) VALUES(:referrer, :refhost, :reftitle);";
				$q .= "INSERT IGNORE INTO url_lookup( url, host, title ) VALUES(:content, :conhost, :contitle);";
				$q .= "INSERT INTO url_map( domain_id, `date`, url_from, url_to ) VALUES( " .
					  ":domain_id, " .
					  "now(), " .
					  "( SELECT url_id FROM url_lookup WHERE url = :referrer )," .
					  "( SELECT url_id FROM url_lookup WHERE url = :content )" .
					  ") ON DUPLICATE KEY UPDATE count = count + 1;";

				//add to browsers -- varchar7
				$q .= "INSERT INTO  browsers( domain_id, `date`, browser ) values( " .
					  ":domain_id, " .
					  "'$date', " .
					  " :browser" .
					  ") ON DUPLICATE KEY UPDATE count = count + 1;";

				//add to os -- varchar6
				$q .= "INSERT INTO `operating_systems` ( domain_id, `date`, os, type) VALUES " .
					  "(:domain_id, '$date', :os, :type) ON DUPLICATE KEY UPDATE count = count + 1;";

				//add to screens -- varch4
				$q .= "INSERT INTO  resolutions( domain_id, `date`, resolution ) VALUES" .
					  "(:domain_id, '$date', :resolution) ON DUPLICATE KEY UPDATE count = count + 1;";

				//add to lang -- char2
				$q .= "INSERT INTO  languages( domain_id, `date`, language ) VALUES " .
					  "(:domain_id, '$date', :language) ON DUPLICATE KEY UPDATE count = count + 1;";

				//add to cache, get insertid
				$q .= "INSERT IGNORE INTO ip_location_cache( `date`, ip ) VALUES ( '$date', INET_ATON(:ipaddr) );";

				//add to map
				$q .= "INSERT INTO map( domain_id, `date`, locId ) VALUES " .
					  "(:domain_id, '$date', ( select locId from ip_location_cache WHERE ip = INET_ATON(:ipaddr) ) ) " .
					  "ON DUPLICATE KEY UPDATE count = count + 1;";

				//add dom_time if available
				if ( $this->data[ 'dom_time' ] ) {
					$q .= "INSERT INTO page_stats( domain_id, `date`, url_id, dom_content_loaded ) VALUES " .
						  "( :domain_id, now(), ( SELECT url_id FROM url_lookup WHERE url = :content), :dom_time ) " .
						  "ON DUPLICATE KEY UPDATE dom_content_loaded = ( dom_content_loaded + :dom_time)/2;";
				}

				$q = $this->getDoctrine()->getConnection()->prepare( $q );

				$q->bindValue( 'referrer', $this->data[ 'referrer' ] );
				$q->bindValue( 'domain_id', $this->domain_id );

				if ( $this->data[ 'referrer' ] == '(direct)' )
					$q->bindValue( 'refhost', '(direct)' );
				else
					$q->bindValue( 'refhost', $this->data[ 'ref_host' ] );

				$q->bindValue( 'reftitle', $this->data[ 'referrer_title' ] );

				$q->bindValue( 'content', $this->data[ 'content' ] );
				$q->bindValue( 'conhost', $this->host );
				$q->bindValue( 'contitle', $this->data[ 'content_title' ] );
				$q->bindValue( 'browser', $this->data[ 'browser' ] );
				$q->bindValue( 'os', $this->data[ 'os' ] );
				$q->bindValue( 'type', $this->data[ 'type' ] );
				$q->bindValue( 'resolution', $this->data[ 'resolution' ] );
				$q->bindValue( 'language', $this->data[ 'lang' ] );
				//$q->bindValue(':ipint', $this->ip2Int($this->data['ip']), \PDO::PARAM_INT);

				$q->bindValue( 'ipaddr', $this->data[ 'ip' ] );
				$q->bindValue( 'dom_time', $this->data[ 'dom_time' ] );
			}
			else {
				//no visit, only update view++
				$q = "UPDATE traffic SET views = views + 1 WHERE domain_id = :domain_id AND `date` = '$fulldate';";

				//add to referrer table
				$q .= "INSERT IGNORE INTO url_lookup( url, host, title ) VALUES (:referrer, :refhost, :reftitle);";
				$q .= "INSERT IGNORE INTO url_lookup( url, host, title ) VALUES (:content, :conhost, :contitle);";
				$q .= "INSERT INTO url_map( domain_id, `date`, url_from, url_to ) VALUES" .
					  "( " .
					  ":domain_id," .
					  "'$date'," .
					  "( SELECT url_id FROM url_lookup WHERE url = :referrer ), " .
					  "( SELECT url_id FROM url_lookup WHERE url = :content ) " .
					  ") ON DUPLICATE KEY UPDATE count = count + 1;";

				//add dom_time if available
				if ( $this->data[ 'dom_time' ] ) {
					$q .= "INSERT INTO page_stats( domain_id, `date`, url_id, dom_content_loaded ) VALUES ( " .
						  ":domain_id, now(), ( SELECT url_id FROM url_lookup WHERE url = :content), :dom_time) " .
						  "ON DUPLICATE KEY UPDATE  dom_content_loaded = ( dom_content_loaded + :dom_time)/2;";

				}

				$q = $this->getDoctrine()->getConnection()->prepare( $q );

				$q->bindValue( 'dom_time', $this->data[ 'dom_time' ] );
				$q->bindValue( 'domain_id', $this->domain_id );

				$q->bindValue( 'referrer', $this->data[ 'referrer' ] );
				$q->bindValue( 'reftitle', $this->data[ 'referrer_title' ] );

				if ( $this->data[ 'referrer' ] == '(direct)' )
					$q->bindValue( 'refhost', '(direct)' );
				else
					$q->bindValue( 'refhost', $this->data[ 'ref_host' ] );

				$q->bindValue( 'content', $this->data[ 'content' ] );
				$q->bindValue( 'conhost', $this->host );
				$q->bindValue( 'contitle', $this->data[ 'content_title' ] );
			}

			$ex = $q->execute();

			$this->data[ 'ex' ]     = $ex;
			$this->data[ 'ipaddr' ] = $this->data[ 'ip' ];

			//If SQL query error, dump to error_info.txt
			if ( $ex == 0 /*|| debug*/ )
				file_put_contents( 'errorInfo-log.txt', print_r( $q->errorInfo(), TRUE ), FILE_APPEND );
		}


		/**
		 *
		 */
		private function searchEngineTerms()
		{

			if ( substr( $this->data[ 'ref_host' ], 0, 7 ) == 'google.' ) {
				//Google Engine
				$url                      = $this->data[ 'referrer' ];
				$this->data[ 'referrer' ] = 'google.com';
				$s                        = str_replace( '+', '%20', $url );
				$parsed                   = parse_url( $s, PHP_URL_QUERY );
				parse_str( $parsed, $query );
				$this->data[ 'referrer_title' ] = empty( $query[ 'q' ] ) ? '(secure)' : 'Search Terms: ' . $query[ 'q' ];

			}
			else if ( substr( $this->data[ 'ref_host' ], 0, 5 ) == 'bing.' ) {
				//Bing Engine
				$url = $this->data[ 'referrer' ];
				//$this->data['referrer'] = 'bing.com';
				$s      = str_replace( '+', '%20', $url );
				$parsed = parse_url( $s, PHP_URL_QUERY );
				parse_str( $parsed, $query );
				$this->data[ 'referrer_title' ] = empty( $query[ 'q' ] ) ? '(secure)' : 'Search Terms: ' . $query[ 'q' ];

			}
			else if ( strpos( $this->data[ 'ref_host' ], 'yahoo.' ) !== FALSE ) {
				//Yahoo Engine
				$url = $this->data[ 'referrer' ];
				//$this->data['referrer'] = 'yahoo.com';
				$s      = str_replace( '+', '%20', $url );
				$parsed = parse_url( $s, PHP_URL_QUERY );
				parse_str( $parsed, $query );
				$this->data[ 'referrer_title' ] = empty( $query[ 'p' ] ) ? '(secure)' : 'Search Terms: ' . $query[ 'p' ];

			}
			else if ( strpos( $this->data[ 'ref_host' ], 'baidu.' ) !== FALSE ) {
				//Baidu Engine
				$url = $this->data[ 'referrer' ];
				//$this->data['referrer'] = 'baidu.com';
				$s      = str_replace( '+', '%20', $url );
				$parsed = parse_url( $s, PHP_URL_QUERY );
				parse_str( $parsed, $query );
				$this->data[ 'referrer_title' ] = empty( $query[ 'wd' ] ) ? '(secure)' : 'Search Terms: ' . $query[ 'wd' ];
			}
		}
	}
