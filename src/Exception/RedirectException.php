<?php
	
	namespace Sixnapps\AnalyticBundle\Exception;
	
	use Exception;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	
	/**
	 * Class RedirectException
	 *
	 * @package Sixnapps\AnalyticBundle\Exception
	 */
	class RedirectException extends Exception
	{
		private $redirectResponse;
		
		
		/**
		 * RedirectException constructor.
		 *
		 * @param RedirectResponse $redirectResponse
		 * @param string           $message
		 * @param int              $code
		 * @param Exception|NULL   $previousException
		 */
		public function __construct(
			RedirectResponse $redirectResponse,
			$message = '',
			$code = 0,
			Exception $previousException = NULL
		)
		{
			$this->redirectResponse = $redirectResponse;
			parent::__construct( $message, $code, $previousException );
		}
		
		
		/**
		 * @return RedirectResponse
		 */
		public function getRedirectResponse()
		{
			return $this->redirectResponse;
		}
	}
