<?php

	namespace Sixnapps\AnalyticBundle\Listener;

	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

	/**
	 * Class InjectTracker
	 *
	 * @package Sixnapps\AnalyticBundle\Listener
	 */
	class InjectTracker
	{

		/**
		 * @param FilterResponseEvent $event
		 */
		public function inject(FilterResponseEvent $event)
		{
			if ( !$event->isMasterRequest() ) {
				return;
			}

			$response = $this->addJsScript( $event->getResponse() );
			$event->setResponse( $response );

		}


		/**
		 * @param Response $response
		 *
		 * @return Response|null
		 */
		private function addJsScript(Response $response)
		{
			$content = $response->getContent();

			if ( strpos( $content, 'let glance' ) ) {
				$response->setContent( $content );
				return $response;
			}

			$script = file_get_contents( dirname( dirname( __FILE__ ) ) . '/Resources/public/js/glance.js' );

			$html = '<script>' .
					'let _glance = Routing.generate( "sixnapps.analytic.tracker", null, true );' .
					$script .
					'</script>';

			$content = str_replace(
				'</body>',
				$html . '</body> ',
				$content
			);

			$response->setContent( $content );

			return $response;
		}
	}
