<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;
	use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

	/**
	 * Class UrlLookup
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 *
	 * @UniqueEntity(
	 *     fields = { "url", "host", "title" },
	 *     errorPath="url",
	 *     message="Duplicate row",
	 * )
	 *
	 */
	class UrlLookup
	{
		/**
		 * @ORM\Id()
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(name="url_id", type="integer", nullable=false)
		 */
		protected $urlId;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="url", type="string", length=255, nullable=true)
		 */
		protected $url;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="host", type="string", length=255, nullable=false)
		 */
		protected $host;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="title", type="string", length=255, nullable=false, options={"default"="x"})
		 */
		protected $title;

		/**
		 * @var bool
		 *
		 * @ORM\Column(name="error", type="boolean", nullable=false, options={"default"="0"})
		 */
		protected $error;

		/**
		 * @var bool
		 *
		 * @ORM\Column(name="star", type="boolean", nullable=false, options={"default"="0"})
		 */
		protected $star;

		/**
		 * @var bool
		 *
		 * @ORM\Column(name="spam", type="boolean", nullable=false, options={"default"="0"})
		 */
		protected $spam;


		/**
		 * @return int
		 */
		public function getUrlId(): int
		{
			return $this->urlId;
		}


		/**
		 * @return string
		 */
		public function getUrl(): string
		{
			return $this->url;
		}


		/**
		 * @param string $url
		 *
		 * @return UrlLookup
		 */
		public function setUrl(string $url): self
		{
			$this->url = $url;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getHost(): string
		{
			return $this->host;
		}


		/**
		 * @param string $host
		 *
		 * @return UrlLookup
		 */
		public function setHost(string $host): self
		{
			$this->host = $host;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getTitle(): string
		{
			return $this->title;
		}


		/**
		 * @param string $title
		 *
		 * @return UrlLookup
		 */
		public function setTitle(string $title): self
		{
			$this->title = $title;

			return $this;
		}


		/**
		 * @return bool
		 */
		public function isError(): bool
		{
			return $this->error;
		}


		/**
		 * @param bool $error
		 *
		 * @return UrlLookup
		 */
		public function setError(bool $error): self
		{
			$this->error = $error;

			return $this;
		}


		/**
		 * @return bool
		 */
		public function isStar(): bool
		{
			return $this->star;
		}


		/**
		 * @param bool $star
		 *
		 * @return UrlLookup
		 */
		public function setStar(bool $star): self
		{
			$this->star = $star;

			return $this;
		}


		/**
		 * @return bool
		 */
		public function isSpam(): bool
		{
			return $this->spam;
		}


		/**
		 * @param bool $spam
		 *
		 * @return UrlLookup
		 */
		public function setSpam(bool $spam): self
		{
			$this->spam = $spam;

			return $this;
		}

	}
