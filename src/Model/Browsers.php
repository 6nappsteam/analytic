<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class Browsers
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class Browsers
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="browser", type="string", length=6, nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $browser;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="count", type="integer", nullable=false, options={"default"="1"})
		 */
		protected $count;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return Browsers
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return Browsers
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;

		}


		/**
		 * @return string
		 */
		public function getBrowser(): string
		{
			return $this->browser;
		}


		/**
		 * @param string $browser
		 *
		 * @return Browsers
		 */
		public function setBrowser(string $browser): self
		{
			$this->browser = $browser;

			return $this;

		}


		/**
		 * @return int
		 */
		public function getCount(): int
		{
			return $this->count;
		}


		/**
		 * @param int $count
		 *
		 * @return Browsers
		 */
		public function setCount(int $count): self
		{
			$this->count = $count;

			return $this;
		}

	}
