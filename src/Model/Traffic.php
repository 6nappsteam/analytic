<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class Traffic
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class Traffic
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="datetime", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="views", type="integer", nullable=false, options={"default"="1"})
		 */
		protected $views;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="visits", type="integer", nullable=false, options={"default"="0"})
		 */
		protected $visits;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="uniques", type="integer", nullable=false, options={"default"="0"})
		 */
		protected $uniques;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return AnalyticTraffic
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return AnalyticTraffic
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getViews(): int
		{
			return $this->views;
		}


		/**
		 * @param int $views
		 *
		 * @return AnalyticTraffic
		 */
		public function setViews(int $views): self
		{
			$this->views = $views;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getVisits(): int
		{
			return $this->visits;
		}


		/**
		 * @param int $visits
		 *
		 * @return AnalyticTraffic
		 */
		public function setVisits(int $visits): self
		{
			$this->visits = $visits;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getUniques(): int
		{
			return $this->uniques;
		}


		/**
		 * @param int $uniques
		 *
		 * @return AnalyticTraffic
		 */
		public function setUniques(int $uniques): self
		{
			$this->uniques = $uniques;

			return $this;
		}

	}
