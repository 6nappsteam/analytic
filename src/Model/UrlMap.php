<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class UrlMap
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class UrlMap
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="url_from", type="integer", nullable=true)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $urlFrom;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="url_to", type="integer", nullable=true)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $urlTo;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="count", type="integer", nullable=false, options={"default"="1"})
		 */
		protected $count;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return UrlMap
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return UrlMap
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getUrlFrom(): int
		{
			return $this->urlFrom;
		}


		/**
		 * @param int $urlFrom
		 *
		 * @return UrlMap
		 */
		public function setUrlFrom(int $urlFrom): self
		{
			$this->urlFrom = $urlFrom;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getUrlTo(): int
		{
			return $this->urlTo;
		}


		/**
		 * @param int $urlTo
		 *
		 * @return UrlMap
		 */
		public function setUrlTo(int $urlTo): self
		{
			$this->urlTo = $urlTo;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getCount(): int
		{
			return $this->count;
		}


		/**
		 * @param int $count
		 *
		 * @return UrlMap
		 */
		public function setCount(int $count): self
		{
			$this->count = $count;

			return $this;
		}

	}
