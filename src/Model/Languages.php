<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class Languages
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class Languages
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="language", type="string", length=5, nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $language;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="count", type="integer", nullable=false, options={"default"="1"})
		 */
		protected $count;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return Languages
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return Languages
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getLanguage(): string
		{
			return $this->language;
		}


		/**
		 * @param string $language
		 *
		 * @return Languages
		 */
		public function setLanguage(string $language): self
		{
			$this->language = $language;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getCount(): int
		{
			return $this->count;
		}


		/**
		 * @param int $count
		 *
		 * @return Languages
		 */
		public function setCount(int $count): self
		{
			$this->count = $count;

			return $this;
		}

	}
