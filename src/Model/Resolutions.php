<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class Resolutions
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class Resolutions
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="resolution", type="string", length=4, nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $resolution;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="count", type="integer", nullable=false, options={"default"="1"})
		 */
		protected $count;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return Resolutions
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return Resolutions
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getResolution(): string
		{
			return $this->resolution;
		}


		/**
		 * @param string $resolution
		 *
		 * @return Resolutions
		 */
		public function setResolution(string $resolution): self
		{
			$this->resolution = $resolution;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getCount(): int
		{
			return $this->count;
		}


		/**
		 * @param int $count
		 *
		 * @return Resolutions
		 */
		public function setCount(int $count): self
		{
			$this->count = $count;

			return $this;
		}

	}
