<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class PageStats
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class PageStats
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="url_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $urlId;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="visit_time", type="integer", nullable=false, options={"default" : 0})
		 */
		protected $visitTime;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="dom_content_loaded", type="integer", nullable=false, options={"default" : 0})
		 */
		protected $domContentLoaded;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return PageStats
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return PageStats
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getUrlId(): int
		{
			return $this->urlId;
		}


		/**
		 * @param int $urlId
		 *
		 * @return PageStats
		 */
		public function setUrlId(int $urlId): self
		{
			$this->urlId = $urlId;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getVisitTime(): int
		{
			return $this->visitTime;
		}


		/**
		 * @param int $visitTime
		 *
		 * @return PageStats
		 */
		public function setVisitTime(int $visitTime): self
		{
			$this->visitTime = $visitTime;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getDomContentLoaded(): int
		{
			return $this->domContentLoaded;
		}


		/**
		 * @param int $domContentLoaded
		 *
		 * @return PageStats
		 */
		public function setDomContentLoaded(int $domContentLoaded): self
		{
			$this->domContentLoaded = $domContentLoaded;

			return $this;
		}


	}
