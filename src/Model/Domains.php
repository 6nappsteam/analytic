<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class AnalyticDomains
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class Domains
	{
		/**
		 * @ORM\Id()
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 */
		protected $id;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 */
		protected $date;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="host", type="string", length=65, nullable=false)
		 */
		protected $host;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="name", type="string", length=65, nullable=false)
		 */
		protected $name;


		/**
		 * @return mixed
		 */
		public function getId()
		{
			return $this->id;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getHost(): string
		{
			return $this->host;
		}


		public function setHost(string $host): self
		{
			$this->host = $host;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getName(): string
		{
			return $this->name;
		}


		public function setName(string $name): self
		{
			$this->name = $name;

			return $this;
		}

	}
