<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class IpLocationCache
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class IpLocationCache
	{
		/**
		 * @ORM\Id()
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(name="locId", type="integer", nullable=false)
		 */
		protected $locid;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 */
		protected $date;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="ip", type="integer", nullable=false, options={"unsigned"=true})
		 */
		protected $ip;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="country", type="string", length=2, nullable=true)
		 */
		protected $country;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="region", type="string", length=65, nullable=true)
		 */
		protected $region;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="city", type="string", length=65, nullable=true)
		 */
		protected $city;

		/**
		 * @var float
		 *
		 * @ORM\Column(name="lat", type="float", precision=10, scale=6, nullable=true)
		 */
		protected $lat;

		/**
		 * @var float
		 *
		 * @ORM\Column(name="long", type="float", precision=10, scale=6, nullable=true)
		 */
		protected $long;


		/**
		 * @return int
		 */
		public function getLocid(): int
		{
			return $this->locid;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return IpLocationCache
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getIp(): int
		{
			return $this->ip;
		}


		/**
		 * @param int $ip
		 *
		 * @return IpLocationCache
		 */
		public function setIp(int $ip): self
		{
			$this->ip = $ip;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getCountry(): string
		{
			return $this->country;
		}


		/**
		 * @param string $country
		 *
		 * @return IpLocationCache
		 */
		public function setCountry(string $country): self
		{
			$this->country = $country;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getRegion(): string
		{
			return $this->region;
		}


		/**
		 * @param string $region
		 *
		 * @return IpLocationCache
		 */
		public function setRegion(string $region): self
		{
			$this->region = $region;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getCity(): string
		{
			return $this->city;
		}


		/**
		 * @param string $city
		 *
		 * @return IpLocationCache
		 */
		public function setCity(string $city): self
		{
			$this->city = $city;

			return $this;
		}


		/**
		 * @return float
		 */
		public function getLat(): float
		{
			return $this->lat;
		}


		/**
		 * @param float $lat
		 *
		 * @return IpLocationCache
		 */
		public function setLat(float $lat): self
		{
			$this->lat = $lat;

			return $this;
		}


		/**
		 * @return float
		 */
		public function getLong(): float
		{
			return $this->long;
		}


		/**
		 * @param float $long
		 *
		 * @return IpLocationCache
		 */
		public function setLong(float $long): self
		{
			$this->long = $long;

			return $this;
		}

	}
