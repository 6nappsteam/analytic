<?php

	namespace Sixnapps\AnalyticBundle\Model;

	use Doctrine\ORM\Mapping as ORM;

	/**
	 * Class OperatingSystems
	 *
	 * @package Sixnapps\AnalyticBundle\Model
	 */
	class OperatingSystems
	{
		/**
		 * @var int
		 *
		 * @ORM\Column(name="domain_id", type="integer", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $domainId;

		/**
		 * @var \DateTime
		 *
		 * @ORM\Column(name="date", type="date", nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $date;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="os", type="string", length=8, nullable=false)
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="NONE")
		 */
		protected $os;

		/**
		 * @var string
		 *
		 * @ORM\Column(name="type", type="string", length=1, nullable=false, options={"default"="u","fixed"=true})
		 */
		protected $type;

		/**
		 * @var int
		 *
		 * @ORM\Column(name="count", type="integer", nullable=false, options={"default"="1"})
		 */
		protected $count;


		/**
		 * @return int
		 */
		public function getDomainId(): int
		{
			return $this->domainId;
		}


		/**
		 * @param int $domainId
		 *
		 * @return OperatingSystems
		 */
		public function setDomainId(int $domainId): self
		{
			$this->domainId = $domainId;

			return $this;
		}


		/**
		 * @return \DateTime
		 */
		public function getDate(): \DateTime
		{
			return $this->date;
		}


		/**
		 * @param \DateTime $date
		 *
		 * @return OperatingSystems
		 */
		public function setDate(\DateTime $date): self
		{
			$this->date = $date;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getOs(): string
		{
			return $this->os;
		}


		/**
		 * @param string $os
		 *
		 * @return OperatingSystems
		 */
		public function setOs(string $os): self
		{
			$this->os = $os;

			return $this;
		}


		/**
		 * @return string
		 */
		public function getType(): string
		{
			return $this->type;
		}


		/**
		 * @param string $type
		 *
		 * @return OperatingSystems
		 */
		public function setType(string $type): self
		{
			$this->type = $type;

			return $this;
		}


		/**
		 * @return int
		 */
		public function getCount(): int
		{
			return $this->count;
		}


		/**
		 * @param int $count
		 *
		 * @return OperatingSystems
		 */
		public function setCount(int $count): self
		{
			$this->count = $count;

			return $this;
		}
	}
