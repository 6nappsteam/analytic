<?php
	
	namespace Sixnapps\AnalyticBundle\EventListener;
	
	use Sixnapps\AnalyticBundle\Exception\RedirectException;
	use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
	
	class RedirectExceptionListener
	{
		public function onKernelException( GetResponseForExceptionEvent $event )
		{
			if ( $event->getException() instanceof RedirectException ) {
				$event->setResponse( $event->getException()->getRedirectResponse() );
			}
		}
	}
