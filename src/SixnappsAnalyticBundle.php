<?php
	namespace Sixnapps\AnalyticBundle;

	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\HttpKernel\Bundle\Bundle;
	
	/**
	 * Class SixnappsAnalyticBundle
	 *
	 * @package Sixnapps\AnalyticBundle
	 */
	class SixnappsAnalyticBundle extends Bundle
	{
		/**
		 * @param ContainerBuilder $container
		 */
		public function build( ContainerBuilder $container )
		{
			parent::build( $container );
		}
	}
