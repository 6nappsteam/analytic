<?php

	namespace Sixnapps\AnalyticBundle\Command;

	use Symfony\Component\Console\Command\Command;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Output\OutputInterface;
	use Symfony\Component\Filesystem\Exception\IOException;
	use Symfony\Component\Filesystem\Filesystem;
	use Symfony\Component\HttpKernel\KernelInterface;

	/**
	 * Class SetUpCommand
	 *
	 * @package Sixnapps\AnalyticBundle\Command
	 */
	class SetUpCommand extends Command
	{
		//The name of the command (the part after 'bin/console')

		/**
		 * @var string
		 */
		protected static $defaultName = 'sixnapps:analytic:install';

		/**
		 * @var KernelInterface
		 */
		protected $kernel;

		/**
		 * @var string
		 */
		protected $root;


		/**
		 * SetUpCommand constructor.
		 *
		 * @param KernelInterface $kernel
		 * @param string|null     $name
		 */
		public function __construct(KernelInterface $kernel, ?string $name = NULL)
		{
			parent::__construct( $name );
			$this->kernel = $kernel;
			$this->root   = $this->kernel->getProjectDir();

		}


		/**
		 *
		 */
		protected function configure()
		{
			$this
				->setDescription( 'Set up the bundle Analytic' )
				->setName( 'sixnapps:analytic:install' )
				->setHelp( 'This command allows you set up the bundle Analytic' )
			;
		}


		/**
		 * @param InputInterface  $input
		 * @param OutputInterface $output
		 *
		 * @return int|void|null
		 */
		protected function execute(InputInterface $input, OutputInterface $output)
		{
			// outputs multiple lines to the console (adding "\n" at the end of each line)
			$output->writeln( '<info>Set up AnalyticBundle</info>' );

			// install Entities
			$this->installEntitites( $output );

			// install Forms
			$this->installTemplates( $output );
		}


		/**
		 * @param OutputInterface $output
		 */
		private function installTemplates(OutputInterface $output)
		{
			$fileSystem = new Filesystem();
			try {
				//Create Folders if doesn't exist
				if ( !$fileSystem->exists( $this->root . '/templates/bundles' ) ) {
					$fileSystem->mkdir( $this->root . '/templates/bundles', 0700 );

				}
				if ( !$fileSystem->exists( $this->root . '/templates/bundles/EasyAdminBundle' ) ) {
					$fileSystem->mkdir( $this->root . '/templates/bundles/EasyAdminBundle', 0700 );
				}
				if ( !$fileSystem->exists( $this->root . '/templates/bundles/EasyAdminBundle/default' ) ) {
					$fileSystem->mkdir( $this->root . '/templates/bundles/EasyAdminBundle/default', 0700 );
				}
				//Create Template file if doesn't exist
				$file = $this->root . '/templates/bundles/EasyAdminBundle/default/layout.html.twig';
				if ( !$fileSystem->exists( $file ) ) {
					$fileSystem->touch( $file, 0700 );
				}
				//Write in Entity file
				$fileSystem->dumpFile(
					$file,
					$this->templateWrite( )
				);

				$output->writeln( '<info>Templates Created</info>' );
			}
			catch ( IOException $exception ) {
				$output->writeln( "an error occurred while creating your directory at " . $exception->getPath() );
			}

		}


		/**
		 * @param OutputInterface $output
		 */
		private function installEntitites(OutputInterface $output)
		{
			$prefix   = 'Analytic';
			$entities = [
				'Browsers'         => 'browsers',
				'Domains'          => 'domains',
				'IpLocationCache'  => 'ip_location_cache',
				'Languages'        => 'languages',
				'Map'              => 'map',
				'OperatingSystems' => 'operating_systems',
				'PageStats'        => 'page_stats',
				'Resolutions'      => 'resolutions',
				'Traffic'          => 'traffic',
				'UrlLookup'        => 'url_lookup',
				'UrlMap'           => 'url_map',
			];

			$fileSystem = new Filesystem();
			try {
				//Create Entity Folder if doesn't exist
				if ( !$fileSystem->exists( $this->root . '/src/Entity' ) ) {
					$fileSystem->mkdir( $this->root . '/src/Entity', 0700 );
				}

				foreach ( $entities as $entity => $name ) {
					//Create Entity file if doesn't exist
					if ( !$fileSystem->exists( $this->root . '/src/Entity/' . $prefix . '' . $entity . '.php' ) ) {
						$fileSystem->touch( $this->root . '/src/Entity/' . $prefix . '' . $entity . '.php', 0700 );
					}
					//Write in Entity file
					$fileSystem->dumpFile( $this->root . '/src/Entity/' . $prefix . '' . $entity . '.php', $this->dataWrite( $prefix, $name, $entity ) );
				}
				$output->writeln( '<info>Entities Created</info>' );
			}
			catch ( IOException $exception ) {
				$output->writeln( "an error occurred while creating your directory at " . $exception->getPath() );
			}

			$output->writeln( '<info>Set up finished</info>' );
		}


		/**
		 * @param $prefix
		 * @param $name
		 * @param $entity
		 *
		 * @return string
		 */
		private function dataWrite($prefix, $name, $entity)
		{
			$data = "<?php\n" .
					"    namespace App\Entity;\n\n" .
					"    use Doctrine\ORM\Mapping as ORM;\n" .
					"    use Sixnapps\AnalyticBundle\Model\\" . $entity . " as Model;\n\n" .
					"    /**\n" .
					"     * " . ucfirst( $prefix ) . ucfirst( $name ) . "\n" .
					"     *\n" .
					"     * @ORM\Table(name=\"$name\")\n" .
					"     * @ORM\Entity\n" .
					"     */\n" .
					"     class " . ucfirst( $prefix ) . ucfirst( $entity ) . " extends Model\n" .
					"     {\n" .
					"     }\n";
			return $data;
		}


		/**
		 * @return string
		 */
		private function templateWrite()
		{
			return "{% extends '@SixnappsExtension/easy_admin/layout.html.twig' %}" ;

		}
	}
