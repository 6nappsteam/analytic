//Data bar graph ex: 44
let dataGraphLoadTime  = [];
//All data of traffic
dataTraffic       = [];
//label of data
let labelGraphLoadTime = [];
//Color for bar
let colorGraphLoadTime = [];

$.ajax( {
	method: "GET",
	url:    routePageTraffic,
} ).done( function ( response ) {
	//Filtre sur les 30 derniers jours
	response.stats = response.stats.filter( function ( el ) {
		let current = new Date();
		current     = current.setDate( current.getDate() - 30 );
		return new Date( el.date ) > current;
	} );

	//Set date du graph
	for ( let i = 0; i < 30; i++ ) {
		let end = 0;
		response.stats.some( function ( item ) {
			//Set jour selon index for
			let date = new Date();
			date     = date.setDate( date.getDate() - i );
			date     = new Date( date );
			date.setHours( 0, 0, 0, 0 );

			//Set jour selon index for -1
			let dateMoins1 = new Date();
			dateMoins1     = dateMoins1.setDate( dateMoins1.getDate() - ( i + 1 ) );
			dateMoins1     = new Date( dateMoins1 );
			dateMoins1.setHours( 0, 0, 0, 0 );

			//If date item compris entre date et date -1 push data
			if ( dateMoins1 < new Date( item.date ) && date >= new Date( item.date ) ) {
				dataGraphLoadTime.push( item.dom_content_loaded );
				dataTraffic.push( item );
				end = 1;
				return true;
			}
		} );
		//Id end 0 pas de données pour ce jour set 0 et empty
		if ( end === 0 ) {
			dataGraphLoadTime.push( 0 );
			dataTraffic.push( [] );
		}
	}

	//Reverse les données
	dataGraphLoadTime  = dataGraphLoadTime.reverse();
	dataTraffic       = dataTraffic.reverse();
	colorGraphLoadTime = colorGraphLoadTime.reverse();

	//Add empty label for hide leabl under data
	for ( let i = 0; i < 30; i++ ) {
		labelGraphLoadTime.push( "" );
	}

	//Construit le graph
	buildGraphDetailLoadTime();
} );

let buildGraphDetailLoadTime = function () {
	let ctx               = document.getElementById( "line_chart_load_time" );
	let line_chart_load_time = new Chart( ctx, {
		type:    "line",
		data:    {
			labels:   labelGraphLoadTime,
			datasets: [ {
				label:           "ms",
				data:            dataGraphLoadTime,
				backgroundColor: '#10b131',
			} ]
		},
		options: {
			legend:   {
				display: false,
			},
			title:    {
				display:  true,
				text:     "Temps de chargement",
				fontSize: 24,
			},
			scales:   {
				yAxes: [ {
					gridLines: {
						display: false,
					},
					ticks:     {
						beginAtZero: true,

					}
				} ],
				xAxes: [ {
					gridLines: {
						display: false,
					},
				} ],
			},
		}
	} );
};


