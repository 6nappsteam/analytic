console.log( "yo" );
let dataPie2        = $( "#pie_custom_2" ).data( "data" );
let buildPieCustom2 = function () {
	let ctx          = document.getElementById( "pie_custom_2" );
	let pie_custom_2 = new Chart( ctx, {
		type:    "pie",
		data:    {
			labels:   dataPie2[ "labels" ],
			datasets: [ {
				data:            dataPie2[ "data" ],
				backgroundColor: dataPie2[ "colors" ],
			} ]
		},
		options: {
			legend: {
				display:  true,
				position: "right"
			},
			title:  {
				display:  true,
				text:     dataPie2[ "title" ],
				fontSize: 24,
			},
		}
	} );
};

buildPieCustom2();


