//Data bar graph ex: 44
let dataGraphView  = [];
//All data of traffic
dataTraffic        = [];
//label of data
let labelGraphView = [];
//Color for bar
let colorGraphView = [];

$.ajax( {
	method: "GET",
	url:    route,
} ).done( function ( response ) {
	if ( response.daily !== undefined ) {
//Filtre sur les 30 derniers jours
		response.daily = response.daily.filter( function ( el ) {
			let current = new Date();
			current     = current.setDate( current.getDate() - 30 );
			return new Date( el.jour ) > current;
		} );

		//Set date du graph
		for ( let i = 0; i < 30; i++ ) {
			let end = 0;
			response.daily.some( function ( item ) {
				//Set jour selon index for
				let date = new Date();
				date     = date.setDate( date.getDate() - i );
				date     = new Date( date );
				date.setHours( 0, 0, 0, 0 );

				//Set jour selon index for -1
				let dateMoins1 = new Date();
				dateMoins1     = dateMoins1.setDate( dateMoins1.getDate() - ( i + 1 ) );
				dateMoins1     = new Date( dateMoins1 );
				dateMoins1.setHours( 0, 0, 0, 0 );

				//If date item compris entre date et date -1 push data
				if ( dateMoins1 < new Date( item.jour ) && date >= new Date( item.jour ) ) {
					dataGraphView.push( item.vues );
					dataTraffic.push( item );
					//Id day == 6 ou 0 =) weekEnd
					if ( new Date( item.jour ).getDay() === 6 || new Date( item.jour ).getDay() === 0 ) {
						colorGraphView.push( "#d5d5d5" );
					} else {
						colorGraphView.push( "#1b892f" );
					}
					end = 1;
					return true;
				}
			} );
			//Id end 0 pas de données pour ce jour set 0 et empty
			if ( end === 0 ) {
				dataGraphView.push( 0 );
				dataTraffic.push( [] );
				colorGraphView.push( "#fff" );
			}
		}

		//Reverse les données
		dataGraphView  = dataGraphView.reverse();
		dataTraffic    = dataTraffic.reverse();
		colorGraphView = colorGraphView.reverse();

		//Add empty label for hide leabl under data
		for ( let i = 0; i < 30; i++ ) {
			labelGraphView.push( "" );
		}

		//Construit le graph
		buildGraphicView();
	} else {
		console.warn('response daily undefined');
	}
} );

let buildGraphicView = function () {
	let ctx                 = document.getElementById( "bar_chart_page_view" );
	let bar_chart_page_view = new Chart( ctx, {
		type:    "bar",
		data:    {
			labels:   labelGraphView,
			datasets: [ {
				label:           "Vues",
				data:            dataGraphView,
				backgroundColor: colorGraphView,
				// borderWidth:     1
			} ]
		},
		options: {
			legend: {
				display: false,
			},
			title:  {
				display:  true,
				text:     "Vues des 30 derniers jours",
				fontSize: 24,
			},
			scales: {
				yAxes: [ {
					gridLines: {
						display: false,
					},
					ticks:     {
						beginAtZero: true,

					}
				} ],
				xAxes: [ {
					gridLines: {
						display: false,
					},
				} ],
			},
		}
	} );
};


