//Data bar graph ex: 44
let dataGraphTraffic  = [];
//All data of traffic
let dataTraffic       = [];
//label of data
let labelGraphTraffic = [];
//Color for bar
let colorGraphTraffic = [];
let route             = Routing.generate( "ajax.traffic" );

$.ajax( {
	method: "GET",
	url:    route,
} ).done( function ( response ) {
	if ( response.daily !== undefined ) {
		//Filtre sur les 30 derniers jours
		response.daily = response.daily.filter( function ( el ) {
			let current = new Date();
			current     = current.setDate( current.getDate() - 30 );
			return new Date( el.jour ) > current;
		} );

		//Set date du graph
		for ( let i = 0; i < 30; i++ ) {
			let end = 0;
			response.daily.some( function ( item ) {
				//Set jour selon index for
				let date = new Date();
				date     = date.setDate( date.getDate() - i );
				date     = new Date( date );
				date.setHours( 0, 0, 0, 0 );

				//Set jour selon index for -1
				let dateMoins1 = new Date();
				dateMoins1     = dateMoins1.setDate( dateMoins1.getDate() - ( i + 1 ) );
				dateMoins1     = new Date( dateMoins1 );
				dateMoins1.setHours( 0, 0, 0, 0 );

				//If date item compris entre date et date -1 push data
				if ( dateMoins1 < new Date( item.jour ) && date >= new Date( item.jour ) ) {
					dataGraphTraffic.push( item.visites );
					dataTraffic.push( item );
					//Id day == 6 ou 0 =) weekEnd
					if ( new Date( item.jour ).getDay() === 6 || new Date( item.jour ).getDay() === 0 ) {
						colorGraphTraffic.push( "#d5d5d5" );
					} else {
						colorGraphTraffic.push( "#1b892f" );
					}
					end = 1;
					return true;
				}
			} );
			//Id end 0 pas de données pour ce jour set 0 et empty
			if ( end === 0 ) {
				dataGraphTraffic.push( 0 );
				dataTraffic.push( [] );
				colorGraphTraffic.push( "#fff" );
			}
		}

		//Reverse les données
		dataGraphTraffic  = dataGraphTraffic.reverse();
		dataTraffic       = dataTraffic.reverse();
		colorGraphTraffic = colorGraphTraffic.reverse();

		//Add empty label for hide leabl under data
		for ( let i = 0; i < 30; i++ ) {
			labelGraphTraffic.push( "" );
		}

		//Construit le graph
		buildGraphTraffic();
	} else {
		console.warn( "Pas de données daily" );
	}
} );

let buildGraphTraffic = function () {
	let ctx               = document.getElementById( "bar_chart_traffic" );
	let bar_chart_traffic = new Chart( ctx, {
		type:    "bar",
		data:    {
			labels:   labelGraphTraffic,
			datasets: [ {
				label:           "Visites",
				data:            dataGraphTraffic,
				backgroundColor: colorGraphTraffic,
				// borderWidth:     1
			} ]
		},
		options: {
			legend:   {
				display: false,
			},
			title:    {
				display:  true,
				text:     "Visites des 30 derniers jours",
				fontSize: 24,
			},
			scales:   {
				yAxes: [ {
					gridLines: {
						display: false,
					},
					ticks:     {
						beginAtZero: true,

					}
				} ],
				xAxes: [ {
					gridLines: {
						display: false,
					},
				} ],
			},
			tooltips: {
				custom:    function ( tooltip ) {
					if ( !tooltip ) return;
					// disable displaying the color box;
					tooltip.displayColors = false;
				},
				callbacks: {
					title: function ( tooltipItem ) {
						return dataTraffic[ tooltipItem[ 0 ].index ].jour;
					},
					label: function ( tooltipItem ) {
						let labels  = [];
						labels[ 0 ] = "Visites		" + dataTraffic[ tooltipItem.index ].visites;
						labels[ 1 ] = "Uniques		" + dataTraffic[ tooltipItem.index ].uniques;
						labels[ 2 ] = "Page Views		" + dataTraffic[ tooltipItem.index ].vues;
						labels[ 3 ] = "Pages / Visit	" + parseInt( dataTraffic[ tooltipItem.index ].vues ) / parseInt( dataTraffic[ tooltipItem.index ].visites );
						return labels;
					},
				}
			},
		}
	} );
};


