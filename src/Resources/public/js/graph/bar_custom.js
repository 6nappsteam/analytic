console.log( "yo" );
let dataBar        = $( "#bar_custom" ).data( "data" );
let buildBarCustom = function () {
	let ctx        = document.getElementById( "bar_custom" );
	let bar_custom = new Chart( ctx, {
		type:    "bar",
		data:    {
			labels:   dataBar[ "labels" ],
			datasets: [ {
				data:            dataBar[ "data" ],
				backgroundColor: dataBar[ "colors" ],
			} ]
		},
		options: {
			legend: {
				display: false,
			},
			title:  {
				display:  true,
				text:     dataBar[ "title" ],
				fontSize: 24,
			},
			scales: {
				yAxes: [ {
					gridLines: {
						display: false,
					},
					ticks:     {
						beginAtZero: true,

					}
				} ],
				xAxes: [ {
					gridLines: {
						display: false,
					},
				} ],
			},
		}
	} );
};

buildBarCustom();


