//Data bar graph ex: 44
let dataGraphVisit  = [];
//All data of traffic
dataTraffic         = [];
//label of data
let labelGraphVisit = [];
//Color for bar
let colorGraphVisit = [];

$.ajax( {
	method: "GET",
	url:    route,
} ).done( function ( response ) {
	if ( response.daily !== undefined ) {
		//Filtre sur les 30 derniers jours
		response.daily = response.daily.filter( function ( el ) {
			let current = new Date();
			current     = current.setDate( current.getDate() - 30 );
			return new Date( el.jour ) > current;
		} );

		//Set date du graph
		for ( let i = 0; i < 30; i++ ) {
			let end = 0;
			response.daily.some( function ( item ) {
				//Set jour selon index for
				let date = new Date();
				date     = date.setDate( date.getDate() - i );
				date     = new Date( date );
				date.setHours( 0, 0, 0, 0 );

				//Set jour selon index for -1
				let dateMoins1 = new Date();
				dateMoins1     = dateMoins1.setDate( dateMoins1.getDate() - ( i + 1 ) );
				dateMoins1     = new Date( dateMoins1 );
				dateMoins1.setHours( 0, 0, 0, 0 );

				//If date item compris entre date et date -1 push data
				if ( dateMoins1 < new Date( item.jour ) && date >= new Date( item.jour ) ) {
					dataGraphVisit.push( Math.trunc( item.vues / 2 ) );
					dataTraffic.push( item );
					//Id day == 6 ou 0 =) weekEnd
					if ( new Date( item.jour ).getDay() === 6 || new Date( item.jour ).getDay() === 0 ) {
						colorGraphVisit.push( "#d5d5d5" );
					} else {
						colorGraphVisit.push( "#1b892f" );
					}
					end = 1;
					return true;
				}
			} );
			//Id end 0 pas de données pour ce jour set 0 et empty
			if ( end === 0 ) {
				dataGraphVisit.push( 0 );
				dataTraffic.push( [] );
				colorGraphVisit.push( "#fff" );
			}
		}

		//Reverse les données
		dataGraphVisit  = dataGraphVisit.reverse();
		dataTraffic     = dataTraffic.reverse();
		colorGraphVisit = colorGraphVisit.reverse();

		//Add empty label for hide leabl under data
		for ( let i = 0; i < 30; i++ ) {
			labelGraphVisit.push( "" );
		}

		//Construit le graph
		buildGraphVisit();
	} else {
		console.warn( "response daily is undefined" );
	}
} );

let buildGraphVisit = function () {
	let ctx                  = document.getElementById( "bar_chart_page_visit" );
	let bar_chart_page_visit = new Chart( ctx, {
		type:    "bar",
		data:    {
			labels:   labelGraphVisit,
			datasets: [ {
				label:           "Moyene de pages",
				data:            dataGraphVisit,
				backgroundColor: colorGraphVisit,
				// borderWidth:     1
			} ]
		},
		options: {
			legend: {
				display: false,
			},
			title:  {
				display:  true,
				text:     "Moyenne de pages par visite",
				fontSize: 24,
			},
			scales: {
				yAxes: [ {
					gridLines: {
						display: false,
					},
					ticks:     {
						beginAtZero: true,

					}
				} ],
				xAxes: [ {
					gridLines: {
						display: false,
					},
				} ],
			},
		}
	} );
};


