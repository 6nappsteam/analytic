console.log( "yo" );
let dataPie1        = $( "#pie_custom_1" ).data( "data" );
let buildPieCustom1 = function () {
	let ctx        = document.getElementById( "pie_custom_1" );
	let pie_custom_1 = new Chart( ctx, {
		type:    "pie",
		data:    {
			labels:   dataPie1[ "labels" ],
			datasets: [ {
				data:            dataPie1[ "data" ],
				backgroundColor: dataPie1[ "colors" ],
			} ]
		},
		options: {
			legend: {
				display:  true,
				position: "right"
			},
			title:  {
				display:  true,
				text:     dataPie1[ "title" ],
				fontSize: 24,
			},
		}
	} );
};

buildPieCustom1();


