//Data bar graph ex: 44
let dataGraphTraffic  = [];
//All data of traffic
let dataTraffic       = [];
//label of data
let labelGraphTraffic = [];
//Color for bar
let colorGraphTraffic = [];

let routePageTraffic = Routing.generate( "ajax.page.traffic", {
	"page": $( "#idPage" ).data( "id" )
} );

$.ajax( {
	method: "GET",
	url:    routePageTraffic,
} ).done( function ( response ) {
	//Filtre sur les 30 derniers jours
	response.traffic = response.traffic.filter( function ( el ) {
		let current = new Date();
		current     = current.setDate( current.getDate() - 30 );
		return new Date( el.date ) > current;
	} );

	//Set date du graph
	for ( let i = 0; i < 30; i++ ) {
		let end = 0;
		response.traffic.some( function ( item ) {
			//Set jour selon index for
			let date = new Date();
			date     = date.setDate( date.getDate() - i );
			date     = new Date( date );
			date.setHours( 0, 0, 0, 0 );

			//Set jour selon index for -1
			let dateMoins1 = new Date();
			dateMoins1     = dateMoins1.setDate( dateMoins1.getDate() - ( i + 1 ) );
			dateMoins1     = new Date( dateMoins1 );
			dateMoins1.setHours( 0, 0, 0, 0 );

			//If date item compris entre date et date -1 push data
			if ( dateMoins1 < new Date( item.date ) && date >= new Date( item.date ) ) {
				dataGraphTraffic.push( item.count );
				dataTraffic.push( item );
				end = 1;
				return true;
			}
		} );
		//Id end 0 pas de données pour ce jour set 0 et empty
		if ( end === 0 ) {
			dataGraphTraffic.push( 0 );
			dataTraffic.push( [] );
		}
	}

	//Reverse les données
	dataGraphTraffic  = dataGraphTraffic.reverse();
	dataTraffic       = dataTraffic.reverse();
	colorGraphTraffic = colorGraphTraffic.reverse();

	//Add empty label for hide leabl under data
	for ( let i = 0; i < 30; i++ ) {
		labelGraphTraffic.push( "" );
	}

	//Construit le graph
	buildGraphDetailPageTraffic();
} );

let buildGraphDetailPageTraffic = function () {
	let ctx               = document.getElementById( "line_chart_traffic" );
	let line_chart_traffic = new Chart( ctx, {
		type:    "line",
		data:    {
			labels:   labelGraphTraffic,
			datasets: [ {
				label:           "Visites",
				data:            dataGraphTraffic,
				backgroundColor: '#10b131',
			} ]
		},
		options: {
			legend:   {
				display: false,
			},
			title:    {
				display:  true,
				text:     "Visites des 30 derniers jours",
				fontSize: 24,
			},
			scales:   {
				yAxes: [ {
					gridLines: {
						display: false,
					},
					ticks:     {
						beginAtZero: true,

					}
				} ],
				xAxes: [ {
					gridLines: {
						display: false,
					},
				} ],
			},
		}
	} );
};


