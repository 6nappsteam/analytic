//Data
let data      = [];
let nbDevices = 0;

$.ajax( {
	method: "GET",
	url:    route,
} ).done( function ( response ) {
	console.log( response );
	if ( response.types !== undefined ){
		nbDevices = parseInt( response.types.desktop ) + parseInt( response.types.tablet ) + parseInt( response.types.mobile );
		data.push( response.types.desktop );
		data.push( response.types.tablet );
		data.push( response.types.mobile );

		//Construit le graph
		buildGraphType();
	} else{
		console.warn('Data type undefined');
	}
} );

let buildGraphType = function () {
	let ctx          = document.getElementById( "donut_device" );
	let donut_device = new Chart( ctx, {
		type:    "doughnut",
		data:    {
			labels:   [ "Bureau", "Tablette", "Mobile" ],
			datasets: [ {
				label:           "Visites",
				data:            data,
				backgroundColor: [
					"#e91e63",
					"#4caf50",
					"#3f51b5"
				],
				// borderWidth:     1
			} ]
		},
		options: {
			legend:   {
				display: true,
				position: 'right'
			},
			title:    {
				display:  true,
				text:     "Appareils utilisés",
				fontSize: 24,
			},
			tooltips: {
				callbacks: {
					label: function ( tooltipItem, e ) {
						return e.labels[ tooltipItem.index ] + ": " + Math.trunc( data[ tooltipItem.index ] / nbDevices * 100 ) + "%";
					},
				}
			},
		}
	} );
};


