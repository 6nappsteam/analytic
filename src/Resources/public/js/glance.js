( function ( ce, d ) {
	//indexOf Fix
	Array.prototype.indexOf || ( Array.prototype.indexOf = function ( d, e ) {
		let a;
		if ( null == this ) throw new TypeError( "\"this\" is null or not defined" );
		let c = Object( this ), b = c.length >>> 0;
		if ( 0 === b ) return -1;
		a = +e || 0;
		Infinity === Math.abs( a ) && ( a = 0 );
		if ( a >= b ) return -1;
		for ( a = Math.max( 0 <= a ? a : b - Math.abs( a ), 0 ); a < b; ) {
			if ( a in c && c[ a ] === d ) return a;
			a++
		}
		return -1
	} );

	let start = ( new Date().getTime() );

	let glance = {
		user_agent:       function () {
			return ( navigator.userAgent || navigator.appVersion || "x" ).toLowerCase();
		},
		//domain: function(){ return (document.domain || 'x').toLowerCase(); },
		referrer:         function () {
			return encodeURIComponent( ( d.referrer ) ? d.referrer : "(direct)" );
		},
		/**
		 * @return {number}
		 */
		DOMContentLoaded: function () {
			if ( typeof performance != "undefined" ) {
				return ( new Date().getTime() ) - performance.timing.navigationStart;
			} else {
				return 0;
			}
		},
		setCookie:        function ( name, value, days ) {
			if ( days ) {
				var date = new Date();
				date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
				var expires = "; expires=" + date.toGMTString();
			} else
				var expires = "";
			document.cookie = name + "=" + value + expires + "; path=/";
		},
		getCookie:        function ( name ) {
			var nameEQ = name + "=";
			var ca     = document.cookie.split( ";" );
			for ( var i = 0; i < ca.length; i++ ) {
				var c = ca[ i ];
				while ( c.charAt( 0 ) == " " )
					c = c.substring( 1, c.length );
				if ( c.indexOf( nameEQ ) == 0 )
					return c.substring( nameEQ.length, c.length );
			}
			return null;
		},
		deleteCookie:     function ( name ) {
			this.setCookie( name, "", -1 );
		},
		today:            function () {
			var date = new Date();
			var addZ = function ( n ) {
				return n < 10 ? "0" + n : "" + n;
			};
			return date.getFullYear().toString() + addZ( ( date.getMonth() + 1 ).toString() ) + addZ( date.getDate().toString() );
		},
		resolution:       function () {
			// 320x240 -- (QVGA), 640x480 -- (VGA),
			// 1280x720 -- 720p (HD), 1920x1080 -- 1080p (Full HD), 4096x2160 -- 4k
			var w = screen.width, h = screen.height;
			if ( w >= 4096 && h >= 2160 ) return 4096;
			else if ( w >= 1920 && h >= 1080 ) return 1920;
			else if ( w >= 1280 && h >= 720 ) return 1280;
			else if ( w >= 640 && h >= 480 ) return 640;
			else if ( w >= 320 && h >= 240 ) return 320;
			else return 0;
		},
		timestamp:        function () {
			return new Date().getTime();
		},
		lang:             function () {
			var lang = navigator.language || navigator.userLanguage || "x";
			var langCode;

			if ( lang.indexOf( "-" ) > -1 ) {
				var split = lang.split( "-" );
				langCode  = split[ 0 ].toUpperCase();
			} else {
				langCode = lang.toUpperCase();
			}

			return langCode;
		},
		title:            function () {
			return encodeURIComponent( document.title ) || "x";
		},
		findBrowser:      function () {
			let list = {
				"ie7":     "MSIE 7.0",
				"ie8":     "MSIE 8.0",
				"ie9":     "MSIE 9.0",
				"ie10":    "MSIE 10.0",
				"ie11":    "rv:11",
				"ie":      "MSIE",
				"o":       "Opera/",
				"firefox":      "Firefox/",
				"chrome":  "Chrome/",
				"panic":   "Coda, like Safari",
				"safari":  "AppleWebKit/",
				"opera":   "Presto/",
			};

			for ( let k in list ) {
				if ( this.user_agent().indexOf( list[ k ].toLowerCase() ) > -1 ) {
					return k;
				}
			}
			return "x";
		},
		findOS:           function () {
			var list = {
				"iphone": "iPhone;",
				"ipod":   "iPod;",
				"ipad":   "iPad;",

				"android": "android",

				"bb10":   "BB10;",
				"bbplay": "PlayBook;",
				"bb":     "BlackBerry",

				"wp": "iemobile",

				"winxp":    "Windows NT 5.1",
				"winvista": "Windows NT 6.0",
				"win7":     "Windows NT 6.1",
				"win8":     "Windows NT 6.2",
				"win8":     "Windows NT 6.3", //win8.1
				"win":      "Windows",

				"Xbox One": "Xbox",

				"mac106":  "Mac OS X 10_6",
				"mac107":  "Mac OS X 10_7",
				"mac107":  "Mac OS X 10.7",
				"mac108":  "Mac OS X 10_8",
				"mac108":  "Mac OS X 10.8",
				"mac109":  "Mac OS X 10_9",
				"mac1010": "Mac OS X 10_10",
				"mac":     "Mac OS X",

				"ubuntu": "Ubuntu;",
				"linux":  "Linux"
			};

			for ( let k in list ) {
				if ( this.user_agent().indexOf( list[ k ].toLowerCase() ) > -1 ) {
					return k;
				}
			}
			return "x";
		},
		deviceType:       function () {
			if ( "mac106|mac107|mac108|mac109|mac1010|mac|win8|win7|winvista|winxp|win|ubuntu|linux".indexOf( this.findOS() ) > -1 )
				return "d";
			else if ( "ipad|bbplay".indexOf( this.findOS() ) > -1 )
				return "t";
			else if ( "android|iphone|ipod|ios|bb10|bb|wp".indexOf( this.findOS() ) > -1 )
				return "m";
			else
				return "u";
		},
		clickOnATags:     function () {
			// console.log( "in function click js" );
			var a = d.getElementsByTagName( "a" );
			for ( var i = 0; i < a.length; i++ ) {
				var clickReq = function ( e, a ) {
					var is_external = a.hostname != window.location.hostname;

					e.preventDefault();

					var link = a.href || "";

					var target = a.target;
					var img    = new Image( 1, 1 );
					img.src    = ce
								 + "?click"
								 + "&ln=" + encodeURIComponent( a.href )
								 + "&pt=" + Math.floor( ( ( new Date().getTime() ) - start ) / 1000 )
								 + "&ex=" + ( +is_external )
								 + "&ts=" + Math.floor( glance.timestamp() / 1000 );
					img.onload = function () {
						if ( target == "_blank" )
							window.open( link, "_blank" );
						else
							location.href = link;
					};
				};

				//--------

				if ( a[ i ].hostname.length > 0 ) {
					if ( a[ i ].attachEvent ) {
						a[ i ].attachEvent( "onclick", function ( e ) {
							clickReq( e, this );
						} );

					} else if ( a[ i ].addEventListener ) {
						a[ i ].addEventListener( "click", function ( e ) {
							clickReq( e, this );
						}, false );
					} else {
						a[ i ].onclick = function ( e ) {
							clickReq( e, this );
						};
					}
				}
			}
		},
		track:            function () {
			// console.log( "in function track js" );
			//cookie = [1st visit].[last visit].[# of visits].[003]
			var u = 0, v = 0;
			if ( this.getCookie( "_glance" ) == null ) {
				//not set,is unique++, visit++, pv++
				var str = this.today() + "." + this.today() + ".1.003";
				this.setCookie( "_glance", str, 365 );
				u = 1, v = 1;
			} else if ( this.getCookie( "_glance" ).split( "." )[ 1 ] != this.today() ) {
				//visit++, pv++
				var split = this.getCookie( "_glance" ).split( "." );
				var str   = split[ 0 ] + "." + this.today() + "." + ( parseInt( split[ 2 ] ) + 1 ) + ".003"
				this.setCookie( "_glance", str, 365 );
				v = 1;
			} else {
				//pv++
				//set, check date
				;
			}

			var img = new Image( 1, 1 );
			img.src = ce
					  + "?content=" + window.location.href
					  + "?l=" + this.lang()
					  + "&ct=" + this.title()
					  //+ '&d=' + this.domain()
					  + "&rf=" + this.referrer()
					  + "&r=" + this.resolution()
					  + "&t=" + this.deviceType()
					  + "&b=" + this.findBrowser()
					  + "&os=" + this.findOS()
					  + "&u=" + u
					  + "&v=" + v
					  + "&dt=" + this.DOMContentLoaded()
					  + "&ts=" + Math.floor( this.timestamp() / 1000 );
			// console.log( "image", img );
			// console.log( "before call js click" );
			img.onload = function () {
				// console.log( "in function js call click" );
				glance.clickOnATags();
			};
		}
	};

	//onload
	if ( d.attachEvent ) {
		d.attachEvent( "onload", function ( e ) {
			glance.track();
		} );
	} else if ( d.addEventListener ) {
		window.addEventListener( "load", function ( e ) {
			glance.track();
		}, false );
	} else {
		d.onload = function ( e ) {
			glance.track()
		};
	}
} )( _glance, document );
