<?php

	namespace Sixnapps\AnalyticBundle\Services;
	use Doctrine\ORM\EntityManagerInterface;
	use \Doctrine\DBAL\DBALException;

	/**
	 * Class ContentServices
	 *
	 * @package Sixnapps\AnalyticBundle\Services
	 */
	class ContentServices
	{
		private $em;
		private $URLServices;


		/**
		 * ContentServices constructor.
		 *
		 * @param EntityManagerInterface $em
		 * @param URLServices            $URLServices
		 */
		public function __construct(EntityManagerInterface $em, URLServices $URLServices)
		{
			$this->em          = $em;
			$this->URLServices = $URLServices;
		}


		/**
		 * @param     $domain_id
		 * @param     $host
		 * @param int $days
		 *
		 * @return string
		 * @throws DBALException
		 */
		public function getDatas($domain_id, $host, $days = 7)
		{
			if ( is_null( $domain_id ) ) {
				return '';
			}
			$sql = [];

			//get sum
			$sql[ 'total' ] = "SELECT sum(count) FROM url_map " .
							  "INNER JOIN url_lookup ON url_to = url_lookup.url_id " .
							  "WHERE url_map.domain_id = {$domain_id} " .
							  "AND host = '{$host}' " .
							  "AND url_map.date > NOW() - INTERVAL :days day;";

			//get top content
			$sql[ 'content' ] = "SELECT url_to as url_id, url_lookup.url, url_lookup.title, url_lookup.error, url_lookup.star, sum(count) pv FROM url_map INNER JOIN url_lookup ON url_to = url_lookup.url_id " .
								"WHERE url_map.domain_id = {$domain_id} " .
								"AND host = '{$host}' " .
								"AND url_map.date > NOW() - INTERVAL :days day " .
								"GROUP BY url_map.url_to ORDER BY pv desc;";

			//get page_time
			$sql[ 'time' ] = "SELECT page_stats.url_id, count(visit_time) AS count, sum(page_stats.visit_time)/count(visit_time) AS avg FROM page_stats INNER JOIN url_lookup ON page_stats.url_id = url_lookup.url_id " .
							 "WHERE domain_id = {$domain_id} " .
							 "AND host = '{$host}' " .
							 "AND date > NOW() - INTERVAL :days day GROUP BY page_stats.url_id";

			$this->URLServices->fillURLTitles();

			//loop thru queries
			foreach ( $sql as $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );

				//bind data
				$q->bindValue( ':days', $days );

				//if error
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );

				//get results
				$sql[ $key ] = $q->fetchAll();
			}

			//format total
			$sql[ 'total' ] = $sql[ 'total' ][ 0 ][ 'sum(count)' ];

			//format time
			$temp = [];
			foreach ( $sql[ 'time' ] as $k => $val ) {
				$temp[ $val[ 'url_id' ] ] = $val[ 'avg' ];
			}
			$sql[ 'time' ] = $temp;

			foreach ( $sql[ 'content' ] as $k => $val ) {
				//add page_time if exists
				if ( array_key_exists( $val[ 'url_id' ], $temp ) ) {
					$sql[ 'content' ][ $k ][ 'avg_time' ] = (float) $sql[ 'time' ][ $val[ 'url_id' ] ];
				}
				else {
					$sql[ 'content' ][ $k ][ 'avg_time' ] = 0;
				}

				//add %
				$sql[ 'content' ][ $k ][ 'perc' ] = round( $val[ 'pv' ] / $sql[ 'total' ], 4 );
			}

			return $sql[ 'content' ];
		}

	}
