<?php
	
	namespace Sixnapps\AnalyticBundle\Services;
	
	use Doctrine\DBAL\DBALException;
	use Doctrine\ORM\EntityManagerInterface;
	
	/**
	 * Class TrafficServices
	 *
	 * @package Sixnapps\AnalyticBundle\Services
	 */
	class TrafficServices
	{
		private $em;
		
		
		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}
		
		
		/**
		 * @param $domain_id
		 *
		 * @return array
		 * @throws DBALException
		 */
		public function getDatas( $domain_id )
		{
			if ( is_null( $domain_id ) ) {
				return [
					'daily' => '',
					'monthly' => '',
					'hourly' => '',
				];
			}
			$sql = [];
			
			$sql[ 'hourly' ] = "SELECT date_format(date, '%Hh') heure, SUM(views) vues, SUM(visits) visites, SUM(uniques) uniques FROM traffic " .
							   "WHERE domain_id = {$domain_id} AND date(date) = date(now()) " .
							   "GROUP BY heure " .
							   "ORDER BY heure DESC;";
			
			$sql[ 'daily' ] = "SELECT date_format(date, '%a %e %b, %Y') jour, sum(views) vues, sum(visits) visites, sum(uniques) uniques FROM traffic " .
							  "WHERE domain_id = {$domain_id} " .
							  "GROUP BY jour " .
							  "ORDER BY jour DESC LIMIT 30;";
			
			$sql[ 'monthly' ] = "SELECT date_format(date, '%M') mois, date_format(date, '%M') annee, sum(views) vues, sum(visits) visites, sum(uniques) uniques
								FROM traffic " .
								"WHERE domain_id = {$domain_id} " .
								"GROUP BY annee, mois " .
								"ORDER BY annee desc, mois desc limit 12;";
			
			//d-desktop, m=mobile, u=?, t=tablet
			$sql[ 'types' ] = "SELECT type, sum(count) count FROM operating_systems " .
							  "WHERE domain_id = {$domain_id}	and date > NOW() - INTERVAL 28 day " .
							  "GROUP BY type;";
			
			//loop thru queries
			foreach ( $sql as $key => $q ) {
				$q = $this->em->getConnection()->prepare( $q );
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );
				$sql[ $key ] = $q->fetchAll();
			}
			
			//fill types
			$types = [ "m" => "mobile", "d" => "desktop", "t" => "tablet", "u" => "unknown" ];
			$temp  = [ "mobile" => 0, "desktop" => 0, "tablet" => 0, "unknown" => 0 ];
			foreach ( $sql[ 'types' ] as $k => $type )
				$temp[ $types[ $type[ 'type' ] ] ] = $type[ 'count' ];
			$sql[ 'types' ] = $temp;
			
			return $sql;
		}
	}
