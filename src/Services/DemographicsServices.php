<?php
	
	namespace Sixnapps\AnalyticBundle\Services;
	
	use Doctrine\DBAL\DBALException;
	use Doctrine\ORM\EntityManagerInterface;
	
	/**
	 * class DemographicsServices
	 *
	 * @package Sixnapps\AnalyticBundle\Services
	 */
	class DemographicsServices
	{
		private $em;
		
		
		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}
		
		
		/**
		 * @param $domain_id
		 * @param $days
		 *
		 * @return array
		 * @throws DBALException
		 */
		public function getDatas( $domain_id, $days )
		{
			if ( is_null( $domain_id ) ) {
				return [
					'browsers' => '',
					'os' => '',
					'resolutions' => '',
					'languages' => ''
				];
			}
			$sql = [];
			
			$sql[ 'browsers' ] = "SELECT browser AS name, sum(count) AS count FROM browsers WHERE domain_id = {$domain_id} " .
								 "AND date > NOW() - INTERVAL :days day GROUP BY browser ORDER BY count DESC;";
			
			$sql[ 'languages' ] = "SELECT language AS name, sum(count) AS count FROM languages WHERE domain_id = {$domain_id} " .
								  "AND date > NOW() - INTERVAL :days day GROUP BY language ORDER BY count DESC;";
			
			$sql[ 'os' ] = "SELECT os AS name, sum(count) AS count FROM operating_systems WHERE domain_id = {$domain_id} " .
						   "AND date > NOW() - INTERVAL :days day GROUP BY os ORDER BY count DESC;";
			
			$sql[ 'resolutions' ] = "SELECT resolution AS name, sum(count) AS count FROM resolutions WHERE domain_id = {$domain_id} " .
									"AND date > NOW() - INTERVAL :days day GROUP BY resolution ORDER BY count DESC;";
			
			//loop thru queries
			foreach ( $sql AS $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );
				
				//bind data
				$q->bindValue( ':days', $days );
				
				//if error
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );
				
				//get results
				$sql[ $key ] = $q->fetchAll();
				
			}
			
			foreach ( $sql as $key => $q ) {
				$total = 0;
				foreach ( $q as $item ) {
					$total += $item[ 'count' ];
				}
				foreach ( $q as $k => $item ) {
					$sql[ $key ][ $k ][ 'perc' ] = round( $item[ 'count' ] / $total, 4 );
				}
			}
			
			return $sql;
		}
	}
