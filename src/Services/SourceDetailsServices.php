<?php

	namespace Sixnapps\AnalyticBundle\Services;

	use Doctrine\ORM\EntityManagerInterface;
	use \Doctrine\DBAL\DBALException;

	class SourceDetailsServices
	{
		private $em;


		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}


		/**
		 * @param     $domain_id
		 * @param     $host
		 * @param int $days
		 *
		 * @return string
		 * @throws DBALException
		 */
		public function getDatas( $domain_id, $host, $days = 7 )
		{
			if ( is_null( $domain_id ) ) {
				return '';
			}
			$sql = [];

			$sql[ 'links' ] = "SELECT l1.url, l1.host, sum(count) AS count FROM url_map inner join url_lookup l1 ON l1.url_id = url_map.url_from " .
							  "WHERE domain_id = {$domain_id} " .
							  "AND l1.host = :host and " .
							  "date > NOW() - INTERVAL :days day GROUP BY l1.url ORDER BY count desc";

			//loop thru queries
			foreach ( $sql as $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );

				$q->bindParam( ':host', $host );
				$q->bindParam( ':days', $days );

				if ( !$q->execute() )
					var_dump( $q->errorInfo() );

				//get results
				$sql[ $key ] = $q->fetchAll();
			}

			return $sql[ 'links' ];
		}
	}
