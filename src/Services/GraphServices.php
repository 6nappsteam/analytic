<?php
	/**
	 * Created by PhpStorm.
	 * User: manu
	 * Date: 08/03/19
	 * Time: 13:48
	 */
	
	namespace Sixnapps\AnalyticBundle\Services;
	
	use Doctrine\ORM\EntityManagerInterface;
	use \Doctrine\DBAL\DBALException;
	
	class GraphServices
	{
		private $em;
		
		
		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}
		
		
		/**
		 * @param $domain_id
		 * @param $days
		 *
		 * @return false|string
		 * @throws DBALException
		 */
		public function getDatas($domain_id, $days)
		{
			if ( is_null( $domain_id ) ) {
				return '';
			}
			$sql = "SELECT t1.url AS fromURL, t1.host AS fromHost, t1.error AS fromErr, t2.url toURL, t2.host AS toHost, t2.error AS toErr, sum(count) count FROM url_map ".
				   "INNER JOIN url_lookup t1 ON url_from = t1.url_id INNER JOIN url_lookup t2 ON url_to = t2.url_id " .
				   "WHERE domain_id = {$domain_id} " .
					"AND date > NOW() - INTERVAL ? day " .
				   "GROUP BY url_from, url_to;";
			
			$q = $this->em->getConnection()->prepare($sql);
			
			$q->bindValue(1, $days);
			
			if( !$q->execute() ) var_dump($q->errorInfo() );
			
			$res = $q->fetchAll(\PDO::FETCH_ASSOC);
			
			return $res;
		}
	}
