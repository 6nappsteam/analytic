<?php

	namespace Sixnapps\AnalyticBundle\Services;

	use Doctrine\ORM\EntityManagerInterface;
	use \Doctrine\DBAL\DBALException;

	class PageDetailsServices
	{
		private $em;


		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}


		/**
		 * @param $domain_id
		 * @param $id
		 *
		 * @return array
		 * @throws DBALException
		 */
		public function getDatas( $domain_id, $id )
		{
			if ( is_null( $domain_id ) ) {
				return [];
			}
			$sql = [];

			$sql[ 'details' ] = "SELECT * FROM url_lookup WHERE url_id = :id;";

			$sql[ 'traffic' ] = "SELECT date_format(date, '%a %b %e, %Y') date, sum(count) AS count FROM url_map " .
								"WHERE domain_id = {$domain_id} AND url_to = :id " .
								"GROUP BY `date` ORDER BY unix_timestamp(date) DESC LIMIT 14;";

			$sql[ 'links_in' ] = "SELECT count, date_format(date, '%a %b %e, %Y') date, urlFROM.url, urlFROM.host FROM url_map " .
								 "INNER JOIN url_lookup urlFROM on urlFROM.url_id = url_FROM " .
								 "WHERE domain_id = {$domain_id} AND url_map.url_to = :id " .
								 "ORDER BY unix_timestamp(date) DESC LIMIT 25;";

			$sql[ 'links_out' ] = "SELECT count, date_format(date, '%a %b %e, %Y') date, urlto.url, urlto.host FROM url_map " .
								  "INNER JOIN url_lookup urlto on urlto.url_id = url_to " .
								  "WHERE domain_id = {$domain_id} AND url_map.url_FROM = :id " .
								  "ORDER BY unix_timestamp(date) DESC LIMIT 25;";

			$sql[ 'stats' ] = "SELECT *, date_format(date, '%a %b %e, %Y') date, visit_time, dom_content_loaded FROM page_stats " .
							  "WHERE domain_id = {$domain_id} AND url_id = :id " .
							  "ORDER BY unix_timestamp(date) DESC LIMIT 14;";

			//loop thru queries
			foreach ( $sql AS $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );

				//bind data
				$q->bindValue( ':id', $id );

				//if error
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );

				//get results
				$sql[ $key ] = $q->fetchAll();
			}

			if (sizeof($sql['details']) > 0){
				$sql[ 'details' ] = $sql[ 'details' ][ 0 ];
			}else{
				$sql['details'] = [];
			}

			return $sql;
		}
	}
