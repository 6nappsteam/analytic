<?php
	/**
	 * Created by PhpStorm.
	 * User: manu
	 * Date: 07/03/19
	 * Time: 12:00
	 */
	
	namespace Sixnapps\AnalyticBundle\Services;
	
	use Doctrine\ORM\EntityManagerInterface;
	use \Doctrine\DBAL\DBALException;
	
	/**
	 * Class MobileServices
	 *
	 * @package Sixnapps\AnalyticBundle\Services
	 */
	class MobileServices
	{
		/**
		 * @var EntityManagerInterface
		 */
		private $em;
		
		/**
		 * @var URLServices
		 */
		private $URLServices;
		
		
		/**
		 * MobileServices constructor.
		 *
		 * @param EntityManagerInterface $em
		 * @param URLServices            $URLServices
		 */
		public function __construct( EntityManagerInterface $em, URLServices $URLServices )
		{
			$this->em          = $em;
			$this->URLServices = $URLServices;
		}
		
		
		/**
		 * @param $domain_id
		 * @param $host
		 * @param $days
		 *
		 * @return null
		 * @throws DBALException
		 */
		public function getDatas( $domain_id, $host, $days )
		{
			if ( is_null( $domain_id ) ) {
				return [];
			}
			$this->URLServices->fillURLTitles();
			
			//traffic
			$sql[ 'today' ] = "SELECT date_format(date, '%a %b %e, %Y') date, sum(views) views, sum(visits) visits, sum(uniques) uniques FROM traffic " .
							  "WHERE domain_id = {$domain_id} " .
							  "GROUP BY date ORDER BY date DESC LIMIT 1;";
			
			$sql[ 'month' ] = "SELECT date_format(date, '%M') month, date_format(date, '%Y') year, sum(views) views, sum(visits) visits, sum(uniques) uniques
			FROM traffic " .
							  "WHERE domain_id = {$domain_id} " .
							  "GROUP BY year, month ORDER BY year DESC, month DESC LIMIT 1;";
			
			$sql[ 'content' ] = "SELECT url_to AS url_id, url_lookup.url, url_lookup.title, url_lookup.error, url_lookup.star, sum(count) pv, sum(count) / (SELECT sum(count) FROM url_map INNER JOIN url_lookup ON url_to = url_lookup.url_id
				WHERE url_map.domain_id = {$domain_id} AND host = '{$host}' AND url_map.date > NOW() - INTERVAL :days day) AS perc FROM url_map " .
								"INNER JOIN url_lookup ON url_to = url_lookup.url_id " .
								"WHERE url_map.domain_id = {$domain_id} " .
								"AND host = '{$host}' " .
								"AND url_map.date > NOW() - INTERVAL :days day " .
								"GROUP BY url_map.url_to ORDER BY pv DESC LIMIT 8;";
			
			$sql[ 'sources' ] = "SELECT l1.url, l1.host, sum(count) AS count, count(distinct l1.url) AS urls, sum(count) / ( SELECT sum(count) FROM url_map " .
								"INNER JOIN url_lookup ON url_FROM = url_lookup.url_id " .
								"WHERE url_map.domain_id = {$domain_id} " .
								"AND host <> '{$host}' " .
								"AND url_map.date > NOW() - INTERVAL :days day) AS perc FROM url_map " .
								"INNER JOIN url_lookup l1 ON l1.url_id = url_map.url_FROM " .
								"WHERE domain_id = {$domain_id} " .
								"AND l1.host <> '{$host}' " .
								"AND date > NOW() - INTERVAL :days day " .
								"GROUP BY l1.host, l1.url ORDER BY count DESC LIMIT 8";
			
			$sql[ 'country' ] = "SELECT country, sum(count) AS count, sum(count) / ( SELECT sum(count) FROM map WHERE domain_id = {$domain_id} AND date > NOW() - INTERVAL :days day ) AS perc FROM map " .
								"INNER JOIN ip_location_cache ON map.locId = ip_location_cache.locId " .
								"WHERE domain_id = {$domain_id} AND map.date > NOW() - INTERVAL :days day " .
								"GROUP BY country ORDER BY count DESC LIMIT 8";
			
			//loop thru queries
			foreach ( $sql as $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );
				
				//bind data
				$q->bindValue( ':days', $days );
				
				//if error
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );
				
				//get results
				$sql[ $key ] = $q->fetchAll();
			}
			
			return $sql;
		}
	}
