<?php

	namespace Sixnapps\AnalyticBundle\Services;

	use Doctrine\ORM\EntityManagerInterface;
	use \Doctrine\DBAL\DBALException;

	class SourcesServices
	{
		private $em;
		private $URLServices;


		/**
		 * SourcesServices constructor.
		 *
		 * @param EntityManagerInterface $em
		 * @param URLServices            $URLServices
		 */
		public function __construct(EntityManagerInterface $em, URLServices $URLServices)
		{
			$this->em          = $em;
			$this->URLServices = $URLServices;
		}


		/**
		 * @param     $domain_id
		 * @param     $host
		 * @param int $days
		 *
		 * @return array
		 * @throws DBALException
		 */
		public function getDatas($domain_id, $host, $days = 7)
		{
			if ( is_null( $domain_id ) ) {
				return [
					'all' => []
				];
			}
			$sql = [];

			$sql[ 'all' ] = "SELECT l1.url_id, l1.url, l1.host, sum(count) AS count, count(distinct l1.url) AS urls FROM url_map INNER JOIN url_lookup l1 ON l1.url_id = url_map.url_from " .
							"WHERE domain_id = {$domain_id} " .
							"AND l1.host <> '{$host}' " .
							"AND date > NOW() - INTERVAL :days day " .
							"GROUP BY l1.host, l1.url_id ORDER BY count DESC;";

			//get sum
			$sql[ 'total' ] = "SELECT sum(count) FROM url_map INNER JOIN url_lookup ON url_from = url_lookup.url_id " .
							  "WHERE url_map.domain_id = {$domain_id} " .
							  "AND host <> '{$host}' " .
							  "AND url_map . date > NOW() - INTERVAL :days day;";

			$this->URLServices->fillURLTitles();

			//loop thru queries
			foreach ( $sql as $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );

				//bind data
				$q->bindValue( ':days', $days );

				//if error
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );

				//get results
				$sql[ $key ] = $q->fetchAll();
			}

			$sql[ 'total' ] = $sql[ 'total' ][ 0 ][ 'sum(count)' ];

			foreach ( $sql[ 'all' ] as $k => $val ) {
				$sql[ 'all' ][ $k ][ 'perc' ] = round( $val[ 'count' ] / $sql[ 'total' ], 4 );
			}

			return $sql;
		}

	}
