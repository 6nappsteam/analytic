<?php
	
	namespace Sixnapps\AnalyticBundle\Services;
	
	use Doctrine\DBAL\DBALException;
	use Doctrine\ORM\EntityManagerInterface;
	
	class MapServices
	{
		private $em;
		
		
		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct( EntityManagerInterface $em )
		{
			$this->em = $em;
		}
		
		
		/**
		 * @param $domain_id
		 * @param $days
		 *
		 * @return array
		 * @throws DBALException
		 */
		public function getDatas( $domain_id, $days )
		{
			if ( is_null( $domain_id ) ) {
				return [
					'coordinate' => '',
					'city' => '',
				];
			}
			$this->fillMap();
			$sql = [];
			
			$sql[ 'country' ] = "SELECT country, sum(count) AS count FROM map INNER JOIN ip_location_cache on map.locId = ip_location_cache.locId " .
								"WHERE domain_id = {$domain_id} AND map.date > NOW() - INTERVAL :days day " .
								"GROUP BY country ORDER BY count DESC";
			
			$sql[ 'city' ] = "SELECT country, region, city, sum(count) AS count FROM map INNER JOIN ip_location_cache on map.locId = ip_location_cache.locId " .
							 "WHERE domain_id = {$domain_id} AND map.date > NOW() - INTERVAL :days day " .
							 "GROUP BY country, city, region ORDER BY count DESC";
			
			$sql[ 'coordinate' ] = "SELECT lat, `long` FROM ip_location_cache WHERE lat != 0 AND `long` != 0;";
			
			//loop thru queries
			foreach ( $sql AS $key => $q ) {
				//prepare
				$q = $this->em->getConnection()->prepare( $q );
				
				//bind data
				$q->bindValue( ':days', $days );
				
				//if error
				if ( !$q->execute() )
					var_dump( $q->errorInfo() );
				
				//get results
				$sql[ $key ] = $q->fetchAll();
				
				//full empty + null values
				foreach ( $sql[ $key ] as $i => $values )
					foreach ( $values as $k => $val )
						$sql[ $key ][ $i ][ $k ] = ( empty( $val ) || is_null( $val ) ) ? "Unknown" : htmlentities( $val,
							ENT_QUOTES, "UTF-8" );
			}
			
			foreach ( $sql as $key => $q ) {
				$total = 0;
				if ( $key != 'coordinate' ) {
					foreach ( $q as $item ) {
						$total += $item[ 'count' ];
					}
					foreach ( $q as $k => $item ) {
						$sql[ $key ][ $k ][ 'perc' ] = round( $item[ 'count' ] / $total, 4 );
					}
				}
				
			}
			
			return $sql;
		}
		
		
		/**
		 * @throws \Doctrine\DBAL\DBALException
		 */
		private function fillMap()
		{
			
			//select data from location_buffer (limit 30 days)
			$q = $this->em->getConnection()
						  ->prepare( "SELECT locId, inet_ntoa(ip) AS ip FROM ip_location_cache WHERE (country = '' OR country IS NULL) AND date > NOW() - INTERVAL 30 DAY ORDER BY date DESC" )
			;
			$q->execute();
			$res = $q->fetchAll( \PDO::FETCH_ASSOC );

//			if ( debug )
//				file_put_contents( 'fillMap-log.txt', print_r( $res, TRUE ), FILE_APPEND );
			
			$options = [ 'http' => [ 'timeout' => 25, 'user_agent' => '' ] ];
			$context = stream_context_create( $options );
			
			foreach ( $res as $row ) {
				$json = @file_get_contents( "http://api.ipstack.com/{$row['ip']}?access_key=7a9f4f04322c36de62296f14454e3d66",
					FALSE, $context );
//				if ( debug )
//					file_put_contents( 'fillMap-log.txt', print_r( $json, TRUE ), FILE_APPEND );
				
				if ( $json === FALSE or empty( $json ) ) {
					return; //API Down, do nothing..
				} else {
					//found, add to DB
					$json = json_decode( $json, TRUE );
					$lat  = is_float( $json[ 'latitude' ] ) ? $json[ 'latitude' ] : 0.0;
					$long = is_float( $json[ 'longitude' ] ) ? $json[ 'longitude' ] : 0.0;
					
					$w = $this->em->getConnection()->prepare(
						"UPDATE ip_location_cache SET country = ?, region = ?, city = ?, lat = $lat, `long` = $long WHERE locId = {$row['locId']};" )
					;
					$w->bindValue( 1, $json[ 'country_code' ] );
					$w->bindValue( 2, $json[ 'region_name' ] );
					$w->bindValue( 3, $json[ 'city' ] );
					$w->execute();
				}
			}
		}
	}
