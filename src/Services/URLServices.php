<?php

	namespace Sixnapps\AnalyticBundle\Services;

	use Doctrine\ORM\EntityManagerInterface;

	/**
	 * Class URLServices
	 *
	 * @package Sixnapps\AnalyticBundle\Services
	 */
	class URLServices
	{
		private $em;


		/**
		 * TrafficServices constructor.
		 *
		 * @param $em
		 */
		public function __construct(EntityManagerInterface $em)
		{
			$this->em = $em;
		}


		/**
		 *
		 */
		public function fillURLTitles()
		{
			$q = $this->em->getConnection()->prepare( "select host, url from url_lookup where title = 'x' order by url_id desc;" );
			$q->execute();
			$res = $q->fetchAll();

			$q = $this->em->getConnection()->prepare( "update url_lookup set title = ?, error = ? where url = ?;" );
			foreach ( $res as $url ) {

				$options = [ 'http' => [ 'timeout' => 15, 'user_agent' => 'Analytic Bundle - get page title' ] ];
				$context = stream_context_create( $options );
				$html    = '';
//
				//if host == chiffres ex: 127.0.0.1
				if ( preg_match( '/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $url[ 'host' ] ) === TRUE ) {

					//if balise title est definie
					if ( preg_match( "/\<title\>(.*)\<\/title\>/", $html, $title ) ) {
						$title = $title[ 1 ];
						$http_error = 0;
					}
					//if balie title non defini
					else {
						$title      = "(Title not found)";
						$http_error = 1;
					}


				}
				//if host diff chiffre ex: https://jesuisunsite.com
				else {
					//if balise title est definie
					if ( preg_match( "/\<title\>(.*)\<\/title\>/", $html, $title ) ) {
						$title = $title[ 1 ];
						$http_error = 0;
					}
					//if balie title non defini
					else {
						$find       = [ 'http://', 'https://', 'www.', $url[ 'host' ] ];
						$replace    = [ '', '', '', '' ];
						$title      = str_replace( $find, $replace, $url[ 'url' ] );
						$http_error = 1;
					}

				}
//				if ( !preg_match( '/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $url[ 'host' ] )
//					 && ( $html = @file_get_contents( $url[ 'url' ], FALSE, $context ) ) === FALSE ) {
//					$find       = [ 'http://', 'https://', 'www.', $url[ 'host' ] ];
//					$replace    = [ '', '', '', '' ];
//					$title      = str_replace( $find, $replace, $url[ 'url' ] );
//					$http_error = 1;
//				}
//				else {
//					if ( preg_match( "/\<title\>(.*)\<\/title\>/", $html, $title ) )
//						$title = $title[ 1 ];
//					else
//						$title = "(title not found)";
//
//					$http_error = 0;
//				}
//
				$q->bindValue( 1, $title );
				$q->bindValue( 2, $http_error );
				$q->bindValue( 3, $url[ 'url' ] );

				$q->execute();
			}
		}
	}
