<?php
	
	namespace Sixnapps\AnalyticBundle\Form\Type;
	
	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\Extension\Core\Type\SubmitType;
	use Symfony\Component\Form\FormBuilderInterface;
	
	class DomainType extends AbstractType
	{
		/**
		 * @param FormBuilderInterface $builder
		 * @param array                $options
		 */
		public function buildForm(FormBuilderInterface $builder, array $options)
		{
			$builder
				->add( 'host', null, [
					'label'=> 'domaine',
					'help' => 'Saissiez le domaine sans http(s) et www.',
				] )
				->add( 'name', null, [
					'label' => 'description'
				] )
				->add('submit', SubmitType::class, [
					'label' => 'Enregistrer',
					'attr' => [
						'class' => 'btn btn-primary pull-right'
					]
				])
			;
		}
	}
