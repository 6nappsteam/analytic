6nApps Email Builder
====================

Intégration de Email builder dans symfony 4.

Branch `master` of this bundle requires at least __PHP 7.1__ and __Symfony 4.2__ components

Installation
============

en console
+ composer require sixnapps/email-builder-bundle
+ bin/console assets:install --symlink

la route:

    sixnapps_analytic:
        resource: "@SixnappsAnalyticBundle/Resources/config/routing.yaml"
